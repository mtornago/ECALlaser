# ECALlaser: analysis of ECAL laser reflections

### CMSSW_14_0_2

Contains code to analyze raw data (both .dat or .root format):

	- src/pgras/ExtractData contains the code (.cc in src, .h in interface. .cfi in python) to produce an output ROOT Tree containing info for each crystal

	- The code can be executed with workdir/extract_data.py

	- laser_ana.cc uses RDataFrame to analyze the ROOT Tree and produce plots


### CMSSW_14_0_4

Contains code to analyze reco data:

	- Reco can be performed from raw data using src/reco_RAW2DIGI_RECO.py

	- src/Demo/DemoAnalyzer contains the code (.cc in plugins) to produce an output ROOT Tree containing info for each event, including ECAL RecHits and Digis

	- The code can be executed with src/Demo/DemoAnalyzer/python/ConfFile_cfg.py

	- src/rechit_ana.cc uses RDataFrame to analyze the ROOT Tree and produce plots
