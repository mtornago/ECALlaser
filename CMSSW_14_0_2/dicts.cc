// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dicts
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "ROOT/RConfig.hxx"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "dicts.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_Dictionary();
   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p = nullptr);
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p);
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p);
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*)
   {
      ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> > *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >", -2, "ROOT/RVec.hxx", 1492,
                  typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >) );
      instance.SetNew(&new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR);
      instance.SetDelete(&delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> > >()));
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*)
   {
      return GenerateInitInstanceLocal(static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*>(nullptr))->GetClass();
      ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p) {
      return  p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> > : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >;
   }
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(Long_t nElements, void *p) {
      return p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >[nElements] : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p) {
      delete (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*>(p));
   }
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p) {
      delete [] (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >*>(p));
   }
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEshortgRsPgR(void *p) {
      typedef ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> > current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >

namespace ROOT {
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_Dictionary();
   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p = nullptr);
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p);
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p);
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*)
   {
      ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> > *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >", -2, "ROOT/RVec.hxx", 1492,
                  typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >) );
      instance.SetNew(&new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR);
      instance.SetDelete(&delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> > >()));
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*)
   {
      return GenerateInitInstanceLocal(static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*>(nullptr))->GetClass();
      ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p) {
      return  p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> > : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >;
   }
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(Long_t nElements, void *p) {
      return p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >[nElements] : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p) {
      delete (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*>(p));
   }
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p) {
      delete [] (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >*>(p));
   }
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEfloatgRsPgR(void *p) {
      typedef ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> > current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >

namespace ROOT {
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_Dictionary();
   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_TClassManip(TClass*);
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p = nullptr);
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(Long_t size, void *p);
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p);
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p);
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*)
   {
      ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> > *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >", -2, "ROOT/RVec.hxx", 1492,
                  typeid(ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >) );
      instance.SetNew(&new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR);
      instance.SetNewArray(&newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR);
      instance.SetDelete(&delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR);
      instance.SetDestructor(&destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> > >()));
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*)
   {
      return GenerateInitInstanceLocal(static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*>(nullptr))->GetClass();
      ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p) {
      return  p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> > : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >;
   }
   static void *newArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new(static_cast<::ROOT::Internal::TOperatorNewHelper*>(p)) ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >[nElements] : new ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p) {
      delete (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*>(p));
   }
   static void deleteArray_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p) {
      delete [] (static_cast<ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >*>(p));
   }
   static void destruct_ROOTcLcLVecOpscLcLRVeclEROOTcLcLVecOpscLcLRVeclEdoublegRsPgR(void *p) {
      typedef ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> > current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >

namespace {
  void TriggerDictionaryInitialization_dicts_Impl() {
    static const char* headers[] = {
"dicts.h",
nullptr
    };
    static const char* includePaths[] = {
"/cvmfs/cms.cern.ch/el8_amd64_gcc12/lcg/root/6.30.03-723f04ba093d0553281d42c7b0f6eee1/include/",
"/afs/cern.ch/user/m/mtornago/laser-reflexions/CMSSW_14_0_2/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "dicts dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
namespace ROOT{namespace VecOps{template <typename T> class __attribute__((annotate(R"ATTRDUMP(__cling__ptrcheck(off))ATTRDUMP"))) __attribute__((annotate("$clingAutoload$ROOT/RVec.hxx")))  __attribute__((annotate("$clingAutoload$dicts.h")))  RVec;
}}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "dicts dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "dicts.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >", payloadCode, "@",
"ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >", payloadCode, "@",
"ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("dicts",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_dicts_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_dicts_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_dicts() {
  TriggerDictionaryInitialization_dicts_Impl();
}
