# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: reco --conditions 140X_dataRun3_HLT_frozen_v3 -s RAW2DIGI,RECO --no_exec --eventcontent RECO --filein file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat
import FWCore.ParameterSet.Config as cms



process = cms.Process('RECO')

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_Data_cff')
process.load('Configuration.StandardSequences.Reconstruction_Data_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load("pgras.ListCollection.ListCollection_cfi")


process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1),
#    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)

# Input source
process.source = cms.Source("NewEventStreamFileReader",
    fileNames = cms.untracked.vstring('file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat')
)

#process.options = cms.untracked.PSet(
#    IgnoreCompletely = cms.untracked.vstring(),
#    Rethrow = cms.untracked.vstring(),
#    TryToContinue = cms.untracked.vstring(),
#    accelerators = cms.untracked.vstring('*'),
#    allowUnscheduled = cms.obsolete.untracked.bool,
#    canDeleteEarly = cms.untracked.vstring(),
#    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
#    dumpOptions = cms.untracked.bool(False),
#    emptyRunLumiMode = cms.obsolete.untracked.string,
#    eventSetup = cms.untracked.PSet(
#        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
#            allowAnyLabel_=cms.required.untracked.uint32
#        ),
#        numberOfConcurrentIOVs = cms.untracked.uint32(0)
#    ),
#    fileMode = cms.untracked.string('FULLMERGE'),
#    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
#    holdsReferencesToDeleteEarly = cms.untracked.VPSet(),
#    makeTriggerResults = cms.obsolete.untracked.bool,
#    modulesToCallForTryToContinue = cms.untracked.vstring(),
#    modulesToIgnoreForDeleteEarly = cms.untracked.vstring(),
#    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
#    numberOfConcurrentRuns = cms.untracked.uint32(1),
#    numberOfStreams = cms.untracked.uint32(0),
#    numberOfThreads = cms.untracked.uint32(1),
#    printDependencies = cms.untracked.bool(False),
#    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
#    throwIfIllegalParameter = cms.untracked.bool(True),
#    wantSummary = cms.untracked.bool(False)
#)


# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '140X_dataRun3_HLT_frozen_v3', '')

# Path and EndPath definitions
process.raw2digi_step = cms.Path(process.ecalDigis*process.listColl)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step)
