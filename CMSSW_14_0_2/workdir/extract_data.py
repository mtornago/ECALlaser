import FWCore.ParameterSet.Config as cms

process = cms.Process("TEST")

#process.source = cms.Source("NewEventStreamFileReader",
#   fileNames = cms.untracked.vstring(
#       ' /store/hidata/HIRun2023A/Cosmics/RAW/v1/000/374/813/00000/cbdbf9ec-7c0e-451a-b5ea-a3e15884e1cb.root',
#       '/store/hidata/HIRun2023A/Cosmics/RAW/v1/000/374/813/00000/4e6c2db9-d000-431e-963a-5bd2fa1348c0.root')
#)

#file_run375029 = [ "/store/hidata/HIRun2023A/Cosmics/RAW/v1/000/375/029/00000/ebe7e34b-52b7-4c5c-a1cb-13fb9cf13de3.root",
#                   "/store/hidata/HIRun2023A/Cosmics/RAW/v1/000/375/029/00000/ab9de0cd-6cf1-45d0-9394-93dc2006d123.root" ]

run_379139_root = ['root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/2beb8743-ac57-4998-bee5-57af6207e983.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/9ac46324-a393-4f30-83dd-9c123fc58c25.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/f62071b0-9758-4012-b081-b2bfe6fab3b2.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/c2d649be-91dd-43cf-a53c-be48680bfca9.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/7217699a-366b-498d-ac7c-70b62d5a64e7.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/4a5cb979-6860-47db-bdeb-e09a3c006647.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/50ccd255-6064-4271-8244-3143ee03e51d.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/5b4a9478-48f3-40e7-8e4d-4639a1559035.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/092f920d-bca1-4a4a-b69b-f455813f6cf3.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/affd30c3-2957-4ca2-9079-b2795b1b2b68.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/3d0a2a26-d45a-4ac1-ac2d-d852e09f81e4.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/b63c1e06-dc67-4e2d-aacc-304cabedc99a.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/8ddbeef9-6ab7-4d27-8d42-6f9da09e3fab.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/f2988907-97c6-4baf-b673-0a24aedfd859.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/c56cbf68-5920-4867-9b50-b65104a27a86.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/0857d8e2-09ef-40b7-b370-6019198b8845.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/50c83744-c0b0-4444-b5dc-f94cf4310352.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/658a2544-2d53-4b7c-bc95-88aeddcff6d7.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/8d0e3205-63d4-4f2f-8b70-567d7664fb64.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/7ad4d4c4-6162-47e8-b529-c758e6a30122.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/27fad456-7bbd-4c1a-9c3f-6a50ad6b49c0.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/ee8449ff-f96b-409d-b87b-eaab9bc2da4a.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/53d9241e-d431-4404-bdcd-fbf75ae3517f.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/de8219c2-6b0f-4ad0-9fcc-acbc529461c9.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/139/00000/40e7bd41-496f-4db2-b3e4-ebfa861c892d.root']

run_379138_root = ['root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/138/00000/f564c060-5091-4b4c-8563-642a4b915e8b.root']

run_379135_root = ['root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/135/00000/7813ca08-265a-469a-85ff-b3011bc3b168.root']

run_379133_root = ['root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/133/00000/7827d496-a428-4549-9be9-78f03890f386.root',
'root://cmsxrootd.fnal.gov///store/data/Run2024B/SpecialHLTPhysics/RAW/v1/000/379/133/00000/8a525afd-e6ba-4941-a377-505de4e82fea.root']

file_run374813 = [ '/store/hidata/HIRun2023A/Cosmics/RAW/v1/000/374/813/00000/cbdbf9ec-7c0e-451a-b5ea-a3e15884e1cb.root',
                   '/store/hidata/HIRun2023A/Cosmics/RAW/v1/000/374/813/00000/4e6c2db9-d000-431e-963a-5bd2fa1348c0.root']

file_run375029 = [ "/store/hidata/HIRun2023A/MinimumBias/RAW/v1/000/375/029/00000/bfd643ea-25dd-4abe-8bb4-c3dd7c422f3f.root",
                   "/store/hidata/HIRun2023A/MinimumBias/RAW/v1/000/375/029/00000/ff67fbaa-1e59-479e-a75b-c74f495cff95.root" ]

file_run378496 = [ "/store/data/Run2024A/SpecialHLTPhysics/RAW/v1/000/378/496/00000/06f105f1-f32f-41d2-91f4-139d208e4264.root",
                   "/store/data/Run2024A/SpecialHLTPhysics/RAW/v1/000/378/496/00000/dfcc1e01-9a1f-4f2e-91a0-1b7e5e28c415.root" ]
file_run378498 = [ "/store/data/Run2024A/SpecialHLTPhysics/RAW/v1/000/378/498/00000/5530c335-8fce-44aa-9572-95bc6759ab66.root",
                   "/store/data/Run2024A/SpecialHLTPhysics/RAW/v1/000/378/498/00000/6a2a0c73-3db8-49e1-acf8-20a6f1166fb0.root" ]

file_run379083 = [
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"file:/eos/cms/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379//083/run379083_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat" ]

file_run379084 = [ "/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0014_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0015_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0016_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0017_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0018_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0019_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0020_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0021_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0022_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0023_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0024_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0025_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0026_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0027_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0028_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0029_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0030_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0031_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0032_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0033_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0034_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0035_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0036_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0037_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0038_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0039_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0040_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0041_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0042_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0043_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0044_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0045_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0046_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0047_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0048_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0049_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0050_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/084/run379084_ls0051_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]

file_run379133 = [
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0014_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0015_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0016_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0017_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0018_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0019_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0020_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0021_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0022_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0023_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0024_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0025_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0026_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0027_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0028_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0029_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0030_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0031_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0032_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0033_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0034_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0035_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0036_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0037_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0038_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0039_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0040_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0041_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0042_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0043_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0044_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0045_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0046_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0047_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0048_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0049_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0050_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/133/run379133_ls0051_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]


file_run379135 = [
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0014_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0015_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0016_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0017_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0018_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0019_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0020_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0021_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0022_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0023_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0024_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0025_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0026_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0027_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0028_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0029_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0030_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0031_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0032_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0033_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0034_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0035_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0036_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0037_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0038_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0039_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0040_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0041_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0042_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0043_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0044_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0045_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0046_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0047_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0048_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0049_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/135/run379135_ls0050_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]


file_run379138 = [
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0014_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0015_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0016_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0017_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0018_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0019_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0020_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0021_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0022_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0023_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0024_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0025_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0026_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0027_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0028_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0029_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0030_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0031_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0032_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0033_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0034_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0035_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0036_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0037_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0038_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0039_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0040_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0041_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0042_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0043_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0044_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0045_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0046_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0047_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0048_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0049_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0050_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]

#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0051_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0052_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0053_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0054_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0055_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0056_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0057_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0058_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0059_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0060_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0061_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0062_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0063_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0064_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0065_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0066_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0067_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0068_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0069_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0070_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0071_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0072_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0073_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0074_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0075_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0076_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0077_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0078_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0079_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0080_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0081_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0082_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0083_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0084_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0085_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0086_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0087_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0088_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0089_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0090_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0091_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0092_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0093_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0094_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0095_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0096_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0097_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0098_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0099_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0100_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0101_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
#"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/138/run379138_ls0102_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]


file_run379139 = [
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0001_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0002_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0003_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0004_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0005_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0006_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0007_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0008_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0009_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0010_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0011_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0012_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0013_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0014_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0015_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0016_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0017_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0018_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0019_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0020_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0021_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0022_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0023_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0024_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0025_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0026_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0027_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0028_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0029_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0030_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0031_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0032_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0033_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0034_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0035_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0036_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0037_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0038_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0039_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0040_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0041_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0042_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0043_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0044_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0045_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0046_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0047_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0048_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0049_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0050_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0051_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0052_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0053_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0054_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0055_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0056_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0057_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0058_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0059_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0060_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0061_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0062_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0063_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0064_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0065_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0066_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0067_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0068_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0069_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0070_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0071_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0072_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0073_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0074_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0075_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0076_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0077_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0078_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0079_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0080_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0081_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0082_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0083_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0084_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0085_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0086_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0087_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0088_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0089_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0090_streamPhysicsSpecialHLTPhysics_StorageManager.dat",
"/store/t0streamer/Data/PhysicsSpecialHLTPhysics/000/379/139/run379139_ls0091_streamPhysicsSpecialHLTPhysics_StorageManager.dat"]


#process.source = cms.Source("NewEventStreamFileReader",
#   fileNames = cms.untracked.vstring(file_run379138)
#                            )

process.source = cms.Source("PoolSource",
                            fileNames = cms.untracked.vstring(run_379135_root)
)

#process.maxEvents = cms.untracked.PSet(
#    input = cms.untracked.int32(10000)
#)

process.options = cms.untracked.PSet(
    numberOfThreads  = cms.untracked.uint32(1)
)


process.MessageLogger = cms.Service("MessageLogger",
     cout = cms.untracked.PSet(
         threshold = cms.untracked.string('WARNING'),
         default = cms.untracked.PSet(
             limit = cms.untracked.int32(0)
         )
     ),
     destinations = cms.untracked.vstring('cout')
 )


process.listColl = cms.EDAnalyzer("ListCollection")

process.load('EventFilter.ScalersRawToDigi.ScalersRawToDigi_cfi')
process.scalersRawToDigi.scalersInputTag = 'rawDataCollector'

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load("Geometry.EcalMapping.EcalMapping_cfi")

from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '140X_dataRun3_Prompt_v2', '')

#process.eegeom = cms.ESSource("EmptyESSource",
#     recordName = cms.string('EcalMappingRcd'),
#     iovIsRunNotTime = cms.bool(True),
#     firstValid = cms.vuint32(1)
#    )
#process.load("EventFilter.EcalRawToDigi.EcalDumpRaw_cfi")

process.load("pgras.ExtractData.ExtractData_cfi")
process.extractData.fedRawDataCollectionTag = cms.InputTag("rawDataCollector")
#process.extractData.fedRawDataCollectionTag = cms.InputTag("hltEcalCalibrationRaw")
#process.extractData.l1aMinX = 1
#process.extractData.l1aMaxX = 600
#
#process.extractData.first_event = 6400
#process.extractData.last_event  = cms.untracked.int32(10)
#process.extractData.dumpAdc = True
process.extractData.dump = False
#process.extractData.l1aHistory = False
#process.extractData.beg_fed_id = 610
#process.extractData.end_fed_id = 610
#process.extractData.eventList = [  ]
#process.extractData.hltPaths = [ "HLT_L1SingleEG10_v2" ]


#process.fedSize = cms.EDAnalyzer("FedSize",
#                                 verbosity = cms.untracked.int32(2))

#process.TFileService = cms.Service("TFileService",
#                                   fileName = cms.string("fedsize.root"))


#process.p = cms.Path(process.scalersRawToDigi*process.extractData)
process.p = cms.Path(process.extractData)
#process.p = cms.Path(process.fedSize*process.extractData)
#process.p = cms.Path(process.listColl)
process.schedule = cms.Schedule(process.p)

