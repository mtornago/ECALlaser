ifeq ($(strip $(PypgrasExtractData)),)
PypgrasExtractData := self/src/pgras/ExtractData/python
src_pgras_ExtractData_python_parent := src/pgras/ExtractData
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/pgras/ExtractData/python)
PypgrasExtractData_files := $(patsubst src/pgras/ExtractData/python/%,%,$(wildcard $(foreach dir,src/pgras/ExtractData/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PypgrasExtractData_LOC_USE := self   
PypgrasExtractData_PACKAGE := self/src/pgras/ExtractData/python
ALL_PRODS += PypgrasExtractData
PypgrasExtractData_INIT_FUNC        += $$(eval $$(call PythonProduct,PypgrasExtractData,src/pgras/ExtractData/python,src_pgras_ExtractData_python))
else
$(eval $(call MultipleWarningMsg,PypgrasExtractData,src/pgras/ExtractData/python))
endif
ALL_COMMONRULES += src_pgras_ExtractData_python
src_pgras_ExtractData_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_pgras_ExtractData_python,src/pgras/ExtractData/python,PYTHON))
