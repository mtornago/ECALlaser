ALL_SUBSYSTEMS+=pgras
subdirs_src_pgras = src_pgras_ExtractData src_pgras_ListCollection
subdirs_src += src_pgras
ALL_PACKAGES += pgras/ExtractData
subdirs_src_pgras_ExtractData := src_pgras_ExtractData_python src_pgras_ExtractData_src
ifeq ($(strip $(PypgrasExtractData)),)
PypgrasExtractData := self/src/pgras/ExtractData/python
src_pgras_ExtractData_python_parent := src/pgras/ExtractData
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/pgras/ExtractData/python)
PypgrasExtractData_files := $(patsubst src/pgras/ExtractData/python/%,%,$(wildcard $(foreach dir,src/pgras/ExtractData/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PypgrasExtractData_LOC_USE := self   
PypgrasExtractData_PACKAGE := self/src/pgras/ExtractData/python
ALL_PRODS += PypgrasExtractData
PypgrasExtractData_INIT_FUNC        += $$(eval $$(call PythonProduct,PypgrasExtractData,src/pgras/ExtractData/python,src_pgras_ExtractData_python))
else
$(eval $(call MultipleWarningMsg,PypgrasExtractData,src/pgras/ExtractData/python))
endif
ALL_COMMONRULES += src_pgras_ExtractData_python
src_pgras_ExtractData_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_pgras_ExtractData_python,src/pgras/ExtractData/python,PYTHON))
ALL_PACKAGES += pgras/ListCollection
subdirs_src_pgras_ListCollection := src_pgras_ListCollection_python src_pgras_ListCollection_src src_pgras_ListCollection_test
ifeq ($(strip $(PypgrasListCollection)),)
PypgrasListCollection := self/src/pgras/ListCollection/python
src_pgras_ListCollection_python_parent := src/pgras/ListCollection
ALL_PYTHON_DIRS += $(patsubst src/%,%,src/pgras/ListCollection/python)
PypgrasListCollection_files := $(patsubst src/pgras/ListCollection/python/%,%,$(wildcard $(foreach dir,src/pgras/ListCollection/python ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
PypgrasListCollection_LOC_USE := self   
PypgrasListCollection_PACKAGE := self/src/pgras/ListCollection/python
ALL_PRODS += PypgrasListCollection
PypgrasListCollection_INIT_FUNC        += $$(eval $$(call PythonProduct,PypgrasListCollection,src/pgras/ListCollection/python,src_pgras_ListCollection_python))
else
$(eval $(call MultipleWarningMsg,PypgrasListCollection,src/pgras/ListCollection/python))
endif
ALL_COMMONRULES += src_pgras_ListCollection_python
src_pgras_ListCollection_python_INIT_FUNC += $$(eval $$(call CommonProductRules,src_pgras_ListCollection_python,src/pgras/ListCollection/python,PYTHON))
