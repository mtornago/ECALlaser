ifeq ($(strip $(pgras/ExtractData)),)
ALL_COMMONRULES += src_pgras_ExtractData_src
src_pgras_ExtractData_src_parent := pgras/ExtractData
src_pgras_ExtractData_src_INIT_FUNC := $$(eval $$(call CommonProductRules,src_pgras_ExtractData_src,src/pgras/ExtractData/src,LIBRARY))
pgrasExtractData := self/pgras/ExtractData
pgras/ExtractData := pgrasExtractData
pgrasExtractData_files := $(patsubst src/pgras/ExtractData/src/%,%,$(wildcard $(foreach dir,src/pgras/ExtractData/src ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
pgrasExtractData_BuildFile    := $(WORKINGDIR)/cache/bf/src/pgras/ExtractData/BuildFile
pgrasExtractData_LOC_USE := self   DataFormats/FEDRawData FWCore/Framework FWCore/MessageLogger FWCore/ParameterSet Geometry/EcalMapping DataFormats/EcalDetId EventFilter/EcalRawToDigi CondFormats/DataRecord 
pgrasExtractData_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,pgrasExtractData,pgrasExtractData,$(SCRAMSTORENAME_LIB),src/pgras/ExtractData/src))
pgrasExtractData_PACKAGE := self/src/pgras/ExtractData/src
ALL_PRODS += pgrasExtractData
pgrasExtractData_CLASS := LIBRARY
pgras/ExtractData_forbigobj+=pgrasExtractData
pgrasExtractData_INIT_FUNC        += $$(eval $$(call Library,pgrasExtractData,src/pgras/ExtractData/src,src_pgras_ExtractData_src,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
endif
