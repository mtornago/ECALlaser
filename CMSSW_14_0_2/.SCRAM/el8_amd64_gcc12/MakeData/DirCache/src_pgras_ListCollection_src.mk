ifeq ($(strip $(pgras/ListCollection)),)
ALL_COMMONRULES += src_pgras_ListCollection_src
src_pgras_ListCollection_src_parent := pgras/ListCollection
src_pgras_ListCollection_src_INIT_FUNC := $$(eval $$(call CommonProductRules,src_pgras_ListCollection_src,src/pgras/ListCollection/src,LIBRARY))
pgrasListCollection := self/pgras/ListCollection
pgras/ListCollection := pgrasListCollection
pgrasListCollection_files := $(patsubst src/pgras/ListCollection/src/%,%,$(wildcard $(foreach dir,src/pgras/ListCollection/src ,$(foreach ext,$(SRC_FILES_SUFFIXES),$(dir)/*.$(ext)))))
pgrasListCollection_BuildFile    := $(WORKINGDIR)/cache/bf/src/pgras/ListCollection/BuildFile
pgrasListCollection_LOC_USE := self   FWCore/Framework 
pgrasListCollection_PRE_INIT_FUNC += $$(eval $$(call edmPlugin,pgrasListCollection,pgrasListCollection,$(SCRAMSTORENAME_LIB),src/pgras/ListCollection/src))
pgrasListCollection_PACKAGE := self/src/pgras/ListCollection/src
ALL_PRODS += pgrasListCollection
pgrasListCollection_CLASS := LIBRARY
pgras/ListCollection_forbigobj+=pgrasListCollection
pgrasListCollection_INIT_FUNC        += $$(eval $$(call Library,pgrasListCollection,src/pgras/ListCollection/src,src_pgras_ListCollection_src,$(SCRAMSTORENAME_BIN),,$(SCRAMSTORENAME_LIB),$(SCRAMSTORENAME_LOGS),edm))
endif
