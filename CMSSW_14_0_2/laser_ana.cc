/*
 * compile with
 *    # once, for the generation of dictionaries
 *    rootcling dicts.cc dicts.h LinkDef.h
 *    # everytime the source code changes
 *    g++ -Wall -O3 laser_ana.cc dicts.cc `root-config --libs --cflags` -lfftw3 -lfftw3_threads -o laser_ana
 */
#include <cstdio>

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"
#include "ROOT/RVec.hxx"
#include <vector>
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "TH2.h"
#include "TF1.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TGraph.h"

#include <TColor.h>

using namespace std;

bool thereisdata;
float ampl_max, ampl_min;

std::vector<int> find_sel_index(std::vector<int> fed, int sel)
{
	size_t size = fed.size();
	std::vector<int> rs;
	for(size_t i=0; i<size; i++){
		if(fed[i]==sel){ 
			rs.push_back(i);
		}
	}
	return rs;
}

std::vector<float> sel_data_f(std::vector<float> data, std::vector<int> indx)
{
	int sd = data.size();
	int si = indx.size();
	std::vector<float> rs(si);
//	fprintf(stdout,"A = {");
	for(int i=0; i<sd; i++){
		for(int j=0; j<si; j++){
			int index = indx[j];
			if(i==index){
				rs[j] = data[i];
	//			fprintf(stdout,"%f ", rs[j]);
				break;
			}
		}
	}
	
//	fprintf(stdout,"}");
	return rs;
	
}

std::vector<int> sel_data_i(std::vector<int> data, std::vector<int> indx)
{
        int sd = data.size();
        int si = indx.size();
        std::vector<int> rs(si);
//      fprintf(stdout,"A = {");
        for(int i=0; i<sd; i++){
                for(int j=0; j<si; j++){
                        int index = indx[j];
                        if(i==index){
                                rs[j] = data[i];
        //                      fprintf(stdout,"%f ", rs[j]);
                                break;
                        }
                }
        }

//      fprintf(stdout,"}");
        return rs;

}

std::vector<vector<double>> sel_data_m(std::vector<vector<double>> data, std::vector<int> indx)
{
        int sd = data.size();
        int si = indx.size();
        std::vector<vector<double>> rs(si);
//      fprintf(stdout,"A = {");
        for(int i=0; i<sd; i++){
                for(int j=0; j<si; j++){
                        int index = indx[j];
                        if(i==index){
                        	rs[j] = data[i];
				break;
                        }
                }
        }

//      fprintf(stdout,"}");
        return rs;

}

std::vector<int> bx_vec(std::vector<int> indx, int bx)
{
	size_t size = indx.size();
        std::vector<int> rs;
	for(size_t i=0; i<size; i++){
		rs.push_back(bx);
	}
	return rs;
}

int istheredata(std::vector<int> indx)
{
	int rs = 1;
	if(indx.size() == 0) rs = 0;

	return rs;
}

std::vector<int> sample_vec()
{
	std::vector<int> rs(10);
	for(int i=0; i<10; i++){
		rs[i] = i;
	}
	return rs;
}

int range_histo(std::vector<double> data){
	
	for(size_t i=0; i<data.size(); i++){
		if(data[i]<ampl_min) ampl_min = data[i];
		if(data[i]>ampl_max) ampl_max = data[i];
	}
	return 0;
}

std::vector<int> filter_vec(std::vector<float> data, float min, float max)
{
	std::vector<int> rs;
	size_t size = data.size();
	for(size_t i=0; i<size; i++){
		float pos = data[i];
		if(abs(pos)<max && abs(pos)>=min) rs.push_back(i);
	}
	return rs;
	

}

int discard_bad_tower(UInt_t fed, int tt, int xtal)
{
	int rs = 0;
	if(fed==611 && tt==26 && xtal==15) rs = 1;
	else if(fed==612 && tt==5 && xtal==23) rs = 1;
	else if(fed==616 && tt==55 && xtal==22) rs = 1;
	else if(fed==616 && tt==27 && xtal==4) rs = 1;
	else if(fed==617 && tt==17 && xtal==20) rs = 1;
	else if(fed==619 && tt==64 && xtal==7) rs = 1;
	else if(fed==619 && tt==58 && xtal==15) rs = 1;
	else if(fed==619 && tt==18 && xtal==17) rs = 1;
	else if(fed==620 && tt==49 && xtal==20) rs = 1;
	else if(fed==623 && tt==48 && xtal==11) rs = 1;
	else if(fed==623 && tt==47 && xtal==13) rs = 1;
	else if(fed==623 && tt==47 && xtal==23) rs = 1;
	else if(fed==630 && tt==51 && xtal==2) rs = 1;
	else if(fed==631 && tt==11 && xtal==22) rs = 1;
	else if(fed==631 && tt==3 && xtal==5) rs = 1;
	else if(fed==631 && tt==54 && xtal==13) rs = 1;
	else if(fed==634 && tt==24 && xtal==23) rs = 1;
	else if(fed==640 && tt==26 && xtal==22) rs = 1;
	else if(fed==643 && tt==53 && xtal==19) rs = 1;
	else if(fed==644 && tt==37 && xtal==9) rs = 1;
	else if(fed==651 && tt==17 && xtal==22) rs = 1;
	else if(fed==651 && tt==29 && xtal==4) rs = 1;
	else if(fed==651 && tt==29 && xtal==13) rs = 1;
	else if(fed==651 && tt==29 && xtal==14) rs = 1;
	else if(fed==651 && tt==29 && xtal==23) rs = 1;
	else if(fed==651 && tt==29 && xtal==24) rs = 1;

	return rs;
}

int main(int argc, char ** argv)
{
        // option handling
        char filename_in[128]               = "15997_raw.root";
        char dirname_out[128]               = ".";
        int multithread = 1;
	int run = 0;
        for (int i = 1; i < argc; ++i) {
                if (argv[i][0] != '-') continue;
                switch (argv[i][1]) {
                        case 'i':
                                sprintf(filename_in, "%s", argv[++i]);
                                break;
                        case 'o':
                                sprintf(dirname_out, "%s", argv[++i]);
                                break;
                        case 'r':
                                run = atoi(argv[++i]);
                                break;
                        case 's':
                                multithread = 0;
                                break;
                                return 1;
                }
        }

        fprintf(stdout, "#           input file: %s  run: %d\n", filename_in, run);

        if (multithread) {
                ROOT::EnableThreadSafety();
                ROOT::EnableImplicitMT();
        }

        gErrorIgnoreLevel = kWarning;
        ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(0);
        ROOT::RDataFrame df("ECAL", filename_in);
        fprintf(stderr, "# --> going to run on %d slot(s).\n", df.GetNSlots());
	
	char name[100], fed_ch[100];


	auto da = df
		.Filter("orbit<13000000")
		.Filter("bx<120")
		.Define("discard",discard_bad_tower,{"fedId","dccCh","xtalId"})
		.Filter("discard==0")
		;
	TCanvas *call = new TCanvas("c_A_vs_BX_all_FEDs");
	sprintf(name,"A_vs_BX_allFEDs");
        auto gall = da.Graph("bx","ampl");
	gall->SetNameTitle(name,name);
	call->cd();
	gall->Draw("A*");

	call->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_allFED.png",run,run));

	for(int i=0; i<54; i++){
		
		thereisdata = 0;
		fprintf(stdout,"Looping on FED number %d\n",i+601);

		sprintf(name,"c_%d",601+i);
                TCanvas *c = new TCanvas(name);
		c->Divide(2);
		sprintf(name,"c_map_%d",601+i);
		TCanvas *cm = new TCanvas(name);
		sprintf(name,"c_pulse_bx_%d",601+i);
		TCanvas *cpbx = new TCanvas(name);
		cpbx->Divide(7,3);
		sprintf(name,"c_ampl_bx_%d",601+i);
		TCanvas *cabx = new TCanvas(name);
                cabx->Divide(7,3);
		sprintf(name,"c_pulse_bx_eta_%d",601+i);
		TCanvas *cpbxeta = new TCanvas(name);
		cpbxeta->Divide(4,3);
		sprintf(name,"c_ampl_bx_eta_%d",601+i);
                TCanvas *cabxeta = new TCanvas(name);
		cabxeta->Divide(4,3);

		sprintf(fed_ch,"%d",601+i);
		auto ds = da
		 	   .Define("fed",fed_ch)
			   .Filter("fedId==fed")
		   	   .Define("isamples",sample_vec)
			   ;

			sprintf(name,"A_vs_BX_FED_%d",601+i);
			auto g = ds.Graph("bx","ampl");
			g->SetNameTitle(name,name);
			c->cd(1);
			g->Draw("A*");

			sprintf(name,"A_vs_eta_FED_%d",601+i);
                        auto ge = ds.Graph("eta","ampl");
                        ge->SetNameTitle(name,name);
                        c->cd(2);
                        ge->Draw("A*");

			c->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d.png",run,run,601+i));

			sprintf(name,"A_vs_eta_FED_%d",601+i);
                        auto map = ds.Profile2D({"ampl_map","",120,1,120,20,-3,3},"bx","eta","ampl");
                        map->SetNameTitle(name,name);
                        cm->cd();
                        map->Draw("COLZ");
			
			cm->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_map.png",run,run,601+i));
		
			auto df_25 = ds.Filter("bx==25")
				;
			auto pulse_bx25 = df_25.Profile1D({"pulse_bx25","",10,1,10},"isamples","adc");
			cpbx->cd(3);
			ampl_min = 9999;
			ampl_max = 0;
			for(int i=0; i<10; ++i){
				if(pulse_bx25->GetBinContent(i)>ampl_max) ampl_max = pulse_bx25->GetBinContent(i);
				if(pulse_bx25->GetBinContent(i)<ampl_min) ampl_min = pulse_bx25->GetBinContent(i);
			}
			pulse_bx25->SetMinimum(ampl_min-5);
			pulse_bx25->SetMaximum(ampl_max+5);
			pulse_bx25->Draw();
			auto habx25 = df_25.Histo1D({"ampl_bx25","",30,0,30},"ampl");
                        cabx->cd(3);
                        habx25->Draw();
                        auto df_26 = ds.Filter("bx==26")
                                ;
			auto pulse_bx26 = df_26.Profile1D({"pulse_bx26","",10,1,10},"isamples","adc");	
			cpbx->cd(4);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx26->GetBinContent(i)>ampl_max) ampl_max = pulse_bx26->GetBinContent(i);
                                if(pulse_bx26->GetBinContent(i)<ampl_min) ampl_min = pulse_bx26->GetBinContent(i);
                        }
                        pulse_bx26->SetMinimum(ampl_min-5);
                        pulse_bx26->SetMaximum(ampl_max+5);
			pulse_bx26->Draw();
			auto habx26 = df_26.Histo1D({"ampl_bx26","",30,0,30},"ampl");
			cabx->cd(4);
			habx26->Draw();
                        auto df_64 = ds.Filter("bx==64")
                                ;
			auto pulse_bx64 = df_64.Profile1D({"pulse_bx64","",10,1,10},"isamples","adc");
                        cpbx->cd(9);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx64->GetBinContent(i)>ampl_max) ampl_max = pulse_bx64->GetBinContent(i);
                                if(pulse_bx64->GetBinContent(i)<ampl_min) ampl_min = pulse_bx64->GetBinContent(i);
                        }
                        pulse_bx64->SetMinimum(ampl_min-5);
                        pulse_bx64->SetMaximum(ampl_max+5);
			pulse_bx64->Draw();
			auto habx64 = df_64.Histo1D({"ampl_bx64","",30,0,30},"ampl");
                        cabx->cd(9);
                        habx64->Draw();
                        auto df_65 = ds.Filter("bx==65")
                                ;
                        auto pulse_bx65 = df_65.Profile1D({"pulse_bx65","",10,1,10},"isamples","adc");
                        cpbx->cd(10);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx65->GetBinContent(i)>ampl_max) ampl_max = pulse_bx65->GetBinContent(i);
                                if(pulse_bx65->GetBinContent(i)<ampl_min) ampl_min = pulse_bx65->GetBinContent(i);
                        }
                        pulse_bx65->SetMinimum(ampl_min-5);
                        pulse_bx65->SetMaximum(ampl_max+5);
                        pulse_bx65->Draw();
			auto habx65 = df_65.Histo1D({"ampl_bx65","",30,0,30},"ampl");
                        cabx->cd(10);
                        habx65->Draw();
                        auto df_66 = ds.Filter("bx==66")
                                ;
			auto pulse_bx66 = df_66.Profile1D({"pulse_bx66","",10,1,10},"isamples","adc");
                        cpbx->cd(11);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx66->GetBinContent(i)>ampl_max) ampl_max = pulse_bx66->GetBinContent(i);
                                if(pulse_bx66->GetBinContent(i)<ampl_min) ampl_min = pulse_bx66->GetBinContent(i);
                        }
                        pulse_bx66->SetMinimum(ampl_min-5);
                        pulse_bx66->SetMaximum(ampl_max+5);
                        pulse_bx66->Draw();
			auto habx66 = df_66.Histo1D({"ampl_bx66","",30,0,30},"ampl");
                        cabx->cd(11);
                        habx66->Draw();
                        auto df_67 = ds.Filter("bx==67")
                                ;
			auto pulse_bx67 = df_67.Profile1D({"pulse_bx67","",10,1,10},"isamples","adc");
                        cpbx->cd(12);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx67->GetBinContent(i)>ampl_max) ampl_max = pulse_bx67->GetBinContent(i);
                                if(pulse_bx67->GetBinContent(i)<ampl_min) ampl_min = pulse_bx67->GetBinContent(i);
                        }
                        pulse_bx67->SetMinimum(ampl_min-5);
                        pulse_bx67->SetMaximum(ampl_max+5);
                        pulse_bx67->Draw();
			auto habx67 = df_67.Histo1D({"ampl_bx67","",30,0,30},"ampl");
                        cabx->cd(12);
                        habx67->Draw();
                        auto df_68 = ds.Filter("bx==68")
                                ;
			auto pulse_bx68 = df_68.Profile1D({"pulse_bx68","",10,1,10},"isamples","adc");
                        cpbx->cd(13);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx68->GetBinContent(i)>ampl_max) ampl_max = pulse_bx68->GetBinContent(i);
                                if(pulse_bx68->GetBinContent(i)<ampl_min) ampl_min = pulse_bx68->GetBinContent(i);
                        }
                        pulse_bx68->SetMinimum(ampl_min-5);
                        pulse_bx68->SetMaximum(ampl_max+5);
                        pulse_bx68->Draw();
			auto habx68 = df_68.Histo1D({"ampl_bx68","",30,0,30},"ampl");
                        cabx->cd(13);
                        habx68->Draw();

                        auto df_88 = ds.Filter("bx==88")
                                ;
			auto pulse_bx88 = df_88.Profile1D({"pulse_bx88","",10,1,10},"isamples","adc");
                        cpbx->cd(15);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx88->GetBinContent(i)>ampl_max) ampl_max = pulse_bx88->GetBinContent(i);
                                if(pulse_bx88->GetBinContent(i)<ampl_min) ampl_min = pulse_bx88->GetBinContent(i);
                        }
                        pulse_bx88->SetMinimum(ampl_min-5);
                        pulse_bx88->SetMaximum(ampl_max+5);
                        pulse_bx88->Draw();
                        auto habx88 = df_88.Histo1D({"ampl_bx88","",30,0,30},"ampl");
                        cabx->cd(15);
                        habx88->Draw();
			auto df_89 = ds.Filter("bx==89")
                                ;
                        auto pulse_bx89 = df_89.Profile1D({"pulse_bx89","",10,1,10},"isamples","adc");
                        cpbx->cd(16);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx89->GetBinContent(i)>ampl_max) ampl_max = pulse_bx89->GetBinContent(i);
                                if(pulse_bx89->GetBinContent(i)<ampl_min) ampl_min = pulse_bx89->GetBinContent(i);
                        }
                        pulse_bx89->SetMinimum(ampl_min-5);
                        pulse_bx89->SetMaximum(ampl_max+5);
                        pulse_bx89->Draw();
			auto habx89 = df_89.Histo1D({"ampl_bx89","",30,0,30},"ampl");
                        cabx->cd(16);
                        habx89->Draw();
                        auto df_90 = ds.Filter("bx==90")
                                ;
			auto pulse_bx90 = df_90.Profile1D({"pulse_bx90","",10,1,10},"isamples","adc");
                        cpbx->cd(17);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx90->GetBinContent(i)>ampl_max) ampl_max = pulse_bx90->GetBinContent(i);
                                if(pulse_bx90->GetBinContent(i)<ampl_min) ampl_min = pulse_bx90->GetBinContent(i);
                        }
                        pulse_bx90->SetMinimum(ampl_min-5);
                        pulse_bx90->SetMaximum(ampl_max+5);
                        pulse_bx90->Draw();
			auto habx90 = df_90.Histo1D({"ampl_bx90","",30,0,30},"ampl");
                        cabx->cd(17);
                        habx90->Draw();
                        auto df_91 = ds.Filter("bx==91")
                                ;
			auto pulse_bx91 = df_91.Profile1D({"pulse_bx91","",10,1,10},"isamples","adc");
                        cpbx->cd(18);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx91->GetBinContent(i)>ampl_max) ampl_max = pulse_bx91->GetBinContent(i);
                                if(pulse_bx91->GetBinContent(i)<ampl_min) ampl_min = pulse_bx91->GetBinContent(i);
                        }
                        pulse_bx91->SetMinimum(ampl_min-5);
                        pulse_bx91->SetMaximum(ampl_max+5);
                        pulse_bx91->Draw();
			auto habx91 = df_91.Histo1D({"ampl_bx91","",30,0,30},"ampl");
                        cabx->cd(18);
                        habx91->Draw();
                        auto df_92 = ds.Filter("bx==92")
                                ;
			auto pulse_bx92 = df_92.Profile1D({"pulse_bx92","",10,1,10},"isamples","adc");
                        cpbx->cd(19);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx92->GetBinContent(i)>ampl_max) ampl_max = pulse_bx92->GetBinContent(i);
                                if(pulse_bx92->GetBinContent(i)<ampl_min) ampl_min = pulse_bx92->GetBinContent(i);
                        }
                        pulse_bx92->SetMinimum(ampl_min-5);
                        pulse_bx92->SetMaximum(ampl_max+5);
                        pulse_bx92->Draw();
                        auto habx92 = df_92.Histo1D({"ampl_bx92","",30,0,30},"ampl");
                        cabx->cd(19);
                        habx92->Draw();
			auto df_93 = ds.Filter("bx==93")
                                ;
			auto pulse_bx93 = df_93.Profile1D({"pulse_bx93","",10,1,10},"isamples","adc");
                        cpbx->cd(20);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx93->GetBinContent(i)>ampl_max) ampl_max = pulse_bx93->GetBinContent(i);
                                if(pulse_bx93->GetBinContent(i)<ampl_min) ampl_min = pulse_bx93->GetBinContent(i);
                        }
                        pulse_bx93->SetMinimum(ampl_min-5);
                        pulse_bx93->SetMaximum(ampl_max+5);
                        pulse_bx93->Draw();
			auto habx93 = df_93.Histo1D({"ampl_bx93","",30,0,30},"ampl");
                        cabx->cd(20);
                        habx93->Draw();
                        auto df_94 = ds.Filter("bx==94")
                                ;
			auto pulse_bx94 = df_94.Profile1D({"pulse_bx94","",10,1,10},"isamples","adc");
                        cpbx->cd(21);
			ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(pulse_bx94->GetBinContent(i)>ampl_max) ampl_max = pulse_bx94->GetBinContent(i);
                                if(pulse_bx94->GetBinContent(i)<ampl_min) ampl_min = pulse_bx94->GetBinContent(i);
                        }
                        pulse_bx94->SetMinimum(ampl_min-5);
                        pulse_bx94->SetMaximum(ampl_max+5);
                        pulse_bx94->Draw();
			auto habx94 = df_94.Histo1D({"ampl_bx94","",30,0,30},"ampl");
                        cabx->cd(21);
                        habx94->Draw();

			cpbx->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_pulse_bx.png",run,run,601+i));
			cpbx->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_pulse_bx.pdf",run,run,601+i));
			cabx->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_ampl_bx.png",run,run,601+i));

			//EB eta [0, 1.5], EE eta [1.5, 3]
			
			double eta_min1, eta_min2, eta_min3, eta_min4, eta_max;
		
			eta_min1 = 0;
			eta_min2 = 0.375;
                        eta_min3 = 0.75;
                        eta_min4 = 1.125;
                        eta_max = 1.5;

			if(i<10 || i>45){
				eta_min1 += 1.5;
				eta_min2 += 1.5;
				eta_min3 += 1.5;
				eta_min4 += 1.5;
				eta_max += 1.5;
			}		

			char eta_min1_str[64], eta_min2_str[64], eta_min3_str[64], eta_min4_str[64], eta_max_str[64];
			sprintf(eta_min1_str,"%f",eta_min1);
			sprintf(eta_min2_str,"%f",eta_min2);
			sprintf(eta_min3_str,"%f",eta_min3);
			sprintf(eta_min4_str,"%f",eta_min4);
			sprintf(eta_max_str,"%f",eta_max);

			fprintf(stdout, "eta range [%f, %f], [%f, %f], [%f, %f], [%f, %f]", eta_min1, eta_min2, eta_min2, eta_min3, eta_min3, eta_min4, eta_min4, eta_max);
			auto df_26_e1 = df_26
					.Define("eta_min",eta_min1_str)
					.Define("eta_max",eta_min2_str)
					.Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
					;
			auto df_26_e2 = df_26
                                        .Define("eta_min",eta_min2_str)
                                        .Define("eta_max",eta_min3_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
			auto df_26_e3 = df_26
                                        .Define("eta_min",eta_min3_str)
                                        .Define("eta_max",eta_min4_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
			auto df_26_e4 = df_26
                                        .Define("eta_min",eta_min4_str)
                                        .Define("eta_max",eta_max_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;

			auto df_66_e1 = df_66
                                        .Define("eta_min",eta_min1_str)
                                        .Define("eta_max",eta_min2_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_66_e2 = df_66
                                        .Define("eta_min",eta_min2_str)
                                        .Define("eta_max",eta_min3_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_66_e3 = df_66
                                        .Define("eta_min",eta_min3_str)
                                        .Define("eta_max",eta_min4_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_66_e4 = df_66
                                        .Define("eta_min",eta_min4_str)
                                        .Define("eta_max",eta_max_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
			auto df_91_e1 = df_91
                                        .Define("eta_min",eta_min1_str)
                                        .Define("eta_max",eta_min2_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_91_e2 = df_91
                                        .Define("eta_min",eta_min2_str)
                                        .Define("eta_max",eta_min3_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_91_e3 = df_91
                                        .Define("eta_min",eta_min3_str)
                                        .Define("eta_max",eta_min4_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;
                        auto df_91_e4 = df_91
                                        .Define("eta_min",eta_min4_str)
                                        .Define("eta_max",eta_max_str)
                                        .Filter("abs(eta)>=eta_min && abs(eta)<eta_max")
                                        ;

			auto p26_e1 = df_26_e1.Profile1D({"pulse_bx26_e1","",10,1,10},"isamples","adc");
			cpbxeta->cd(1);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p26_e1->GetBinContent(i)>ampl_max) ampl_max = p26_e1->GetBinContent(i);
                                if(p26_e1->GetBinContent(i)<ampl_min) ampl_min = p26_e1->GetBinContent(i);
                        }
                        p26_e1->SetMinimum(ampl_min-5);
                        p26_e1->SetMaximum(ampl_max+5);
                        p26_e1->Draw();
			auto p26_e2 = df_26_e2.Profile1D({"pulse_bx26_e2","",10,1,10},"isamples","adc");
                        cpbxeta->cd(2);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p26_e2->GetBinContent(i)>ampl_max) ampl_max = p26_e2->GetBinContent(i);
                                if(p26_e2->GetBinContent(i)<ampl_min) ampl_min = p26_e2->GetBinContent(i);
                        }
                        p26_e2->SetMinimum(ampl_min-5);
                        p26_e2->SetMaximum(ampl_max+5);
                        p26_e2->Draw();
			auto p26_e3 = df_26_e3.Profile1D({"pulse_bx26_e3","",10,1,10},"isamples","adc");
                        cpbxeta->cd(3);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p26_e3->GetBinContent(i)>ampl_max) ampl_max = p26_e3->GetBinContent(i);
                                if(p26_e3->GetBinContent(i)<ampl_min) ampl_min = p26_e3->GetBinContent(i);
                        }
                        p26_e3->SetMinimum(ampl_min-5);
                        p26_e3->SetMaximum(ampl_max+5);
                        p26_e3->Draw();
			auto p26_e4 = df_26_e4.Profile1D({"pulse_bx26_e4","",10,1,10},"isamples","adc");
                        cpbxeta->cd(4);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p26_e4->GetBinContent(i)>ampl_max) ampl_max = p26_e4->GetBinContent(i);
                                if(p26_e4->GetBinContent(i)<ampl_min) ampl_min = p26_e4->GetBinContent(i);
                        }
                        p26_e4->SetMinimum(ampl_min-5);
                        p26_e4->SetMaximum(ampl_max+5);
                        p26_e4->Draw();

			auto p66_e1 = df_66_e1.Profile1D({"pulse_bx66_e1","",10,1,10},"isamples","adc");
                        cpbxeta->cd(5);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p66_e1->GetBinContent(i)>ampl_max) ampl_max = p66_e1->GetBinContent(i);
                                if(p66_e1->GetBinContent(i)<ampl_min) ampl_min = p66_e1->GetBinContent(i);
                        }
                        p66_e1->SetMinimum(ampl_min-5);
                        p66_e1->SetMaximum(ampl_max+5);
                        p66_e1->Draw();
			auto p66_e2 = df_66_e2.Profile1D({"pulse_bx66_e2","",10,1,10},"isamples","adc");
                        cpbxeta->cd(6);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p66_e2->GetBinContent(i)>ampl_max) ampl_max = p66_e2->GetBinContent(i);
                                if(p66_e2->GetBinContent(i)<ampl_min) ampl_min = p66_e2->GetBinContent(i);
                        }
                        p66_e2->SetMinimum(ampl_min-5);
                        p66_e2->SetMaximum(ampl_max+5);
                        p66_e2->Draw();
                        auto p66_e3 = df_66_e3.Profile1D({"pulse_bx66_e3","",10,1,10},"isamples","adc");
                        cpbxeta->cd(7);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p66_e3->GetBinContent(i)>ampl_max) ampl_max = p66_e3->GetBinContent(i);
                                if(p66_e3->GetBinContent(i)<ampl_min) ampl_min = p66_e3->GetBinContent(i);
                        }
                        p66_e3->SetMinimum(ampl_min-5);
                        p66_e3->SetMaximum(ampl_max+5);
                        p66_e3->Draw();
                        auto p66_e4 = df_66_e4.Profile1D({"pulse_bx66_e4","",10,1,10},"isamples","adc");
                        cpbxeta->cd(8);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p66_e4->GetBinContent(i)>ampl_max) ampl_max = p66_e4->GetBinContent(i);
                                if(p66_e4->GetBinContent(i)<ampl_min) ampl_min = p66_e4->GetBinContent(i);
                        }
                        p66_e4->SetMinimum(ampl_min-5);
                        p66_e4->SetMaximum(ampl_max+5);
                        p66_e4->Draw();

			auto p91_e1 = df_91_e1.Profile1D({"pulse_bx91_e1","",10,1,10},"isamples","adc");
                        cpbxeta->cd(9);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p91_e1->GetBinContent(i)>ampl_max) ampl_max = p91_e1->GetBinContent(i);
                                if(p91_e1->GetBinContent(i)<ampl_min) ampl_min = p91_e1->GetBinContent(i);
                        }
                        p91_e1->SetMinimum(ampl_min-5);
                        p91_e1->SetMaximum(ampl_max+5);
                        p91_e1->Draw();
                        auto p91_e2 = df_91_e2.Profile1D({"pulse_bx91_e2","",10,1,10},"isamples","adc");
                        cpbxeta->cd(10);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p91_e2->GetBinContent(i)>ampl_max) ampl_max = p91_e2->GetBinContent(i);
                                if(p91_e2->GetBinContent(i)<ampl_min) ampl_min = p91_e2->GetBinContent(i);
                        }
                        p91_e2->SetMinimum(ampl_min-5);
                        p91_e2->SetMaximum(ampl_max+5);
                        p91_e2->Draw();
                        auto p91_e3 = df_91_e3.Profile1D({"pulse_bx91_e3","",10,1,10},"isamples","adc");
                        cpbxeta->cd(11);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p91_e3->GetBinContent(i)>ampl_max) ampl_max = p91_e3->GetBinContent(i);
                                if(p91_e3->GetBinContent(i)<ampl_min) ampl_min = p91_e3->GetBinContent(i);
                        }
                        p91_e3->SetMinimum(ampl_min-5);
                        p91_e3->SetMaximum(ampl_max+5);
                        p91_e3->Draw();
                        auto p91_e4 = df_91_e4.Profile1D({"pulse_bx91_e4","",10,1,10},"isamples","adc");
                        cpbxeta->cd(12);
                        ampl_min = 9999;
                        ampl_max = 0;
                        for(int i=0; i<10; ++i){
                                if(p91_e4->GetBinContent(i)>ampl_max) ampl_max = p91_e4->GetBinContent(i);
                                if(p91_e4->GetBinContent(i)<ampl_min) ampl_min = p91_e4->GetBinContent(i);
                        }
                        p91_e4->SetMinimum(ampl_min-5);
                        p91_e4->SetMaximum(ampl_max+5);
                        p91_e4->Draw();

			auto ha26_1 = df_26_e1.Histo1D({"ampl_bx26_e1","",30,0,30},"ampl");
                        cabxeta->cd(1);
                        ha26_1->Draw();
                        auto ha26_2 = df_26_e2.Histo1D({"ampl_bx26_e2","",30,0,30},"ampl");
                        cabxeta->cd(2);
                        ha26_2->Draw();
                        auto ha26_3 = df_26_e3.Histo1D({"ampl_bx26_e3","",30,0,30},"ampl");
                        cabxeta->cd(3);
                        ha26_3->Draw();
                        auto ha26_4 = df_26_e4.Histo1D({"ampl_bx26_e4","",30,0,30},"ampl");
                        cabxeta->cd(4);
                        ha26_4->Draw();

                        auto ha66_1 = df_66_e1.Histo1D({"ampl_bx66_e1","",30,0,30},"ampl");
                        cabxeta->cd(5);
                        ha66_1->Draw();
                        auto ha66_2 = df_66_e2.Histo1D({"ampl_bx66_e2","",30,0,30},"ampl");
                        cabxeta->cd(6);
                        ha66_2->Draw();
                        auto ha66_3 = df_66_e3.Histo1D({"ampl_bx66_e3","",30,0,30},"ampl");
                        cabxeta->cd(7);
                        ha66_3->Draw();
                        auto ha66_4 = df_66_e4.Histo1D({"ampl_bx66_e4","",30,0,30},"ampl");
                        cabxeta->cd(8);
                        ha66_4->Draw();

                        auto ha91_1 = df_91_e1.Histo1D({"ampl_bx91_e1","",30,0,30},"ampl");
                        cabxeta->cd(9);
                        ha91_1->Draw();
                        auto ha91_2 = df_91_e2.Histo1D({"ampl_bx91_e2","",30,0,30},"ampl");
                        cabxeta->cd(10);
                        ha91_2->Draw();
                        auto ha91_3 = df_91_e3.Histo1D({"ampl_bx91_e3","",30,0,30},"ampl");
                        cabxeta->cd(11);
                        ha91_3->Draw();
                        auto ha91_4 = df_91_e4.Histo1D({"ampl_bx91_e4","",30,0,30},"ampl");
                        cabxeta->cd(12);
                        ha91_4->Draw();

			cpbxeta->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_pulse_bx_eta.png",run,run,601+i));
                        cpbxeta->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_pulse_bx_eta.pdf",run,run,601+i));
			cabxeta->Print(Form("/eos/user/m/mtornago/www/ECALlaserDQM/%d/run%d_FED%d_ampl_bx_eta.png",run,run,601+i));
	}
	
        return 0;
}
