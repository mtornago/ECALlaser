//emacs settings:-*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil -*-
/*
 *
 * Author: Ph Gras. CEA/IRFU - Saclay
 *
 */

#include "FWCore/PluginManager/interface/ModuleDef.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "pgras/ExtractData/interface/ExtractData.h"

DEFINE_FWK_MODULE(ExtractData);
