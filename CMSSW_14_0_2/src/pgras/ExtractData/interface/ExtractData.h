/*
 *
 * Author: Ph Gras. CEA/IRFU - Saclay
 */

#ifndef EXTRACTDATA_H
#define EXTRACTDATA_H

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cinttypes>
//#include "pgras/PGUtilities/interface/PGHisto.h"

#include "DataFormats/FEDRawData/interface/FEDRawDataCollection.h"
#include "DataFormats/Scalers/interface/L1AcceptBunchCrossing.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDAnalyzer.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "DataFormats/Common/interface/TriggerResults.h"  // Classes needed to print trigger results
#include "EventFilter/EcalRawToDigi/interface/EcalElectronicsMapper.h"
#include "EventFilter/EcalRawToDigi/interface/DCCDataUnpacker.h"
#include "DataFormats/EcalDetId/interface/EBDetId.h"
#include "DataFormats/EcalDetId/interface/EEDetId.h"
#include "DataFormats/EcalDetId/interface/EcalElectronicsId.h"
#include "Geometry/EcalMapping/interface/EcalMappingRcd.h"
#include <FWCore/Framework/interface/ESWatcher.h>
#include "CondFormats/EcalObjects/interface/EcalChannelStatus.h"
#include "CondFormats/DataRecord/interface/EcalChannelStatusRcd.h"

#include "TFile.h"
#include "TTree.h"

/**
 * Utility to dump ECAL Raw data. Hexadecimal dump is accompagned with a side by
 * data interpretention.
 *
 * The script test/dumpRaw can be used to run this module. E. g.:
 *  dumpRaw /store/..../data_file.root
 * Run dumpRaw -h to get help on this script.
 *
 * Author: Ph. Gras CEA/IRFU Saclay
 *
 */
class ExtractData : public edm::stream::EDAnalyzer<> {
//  struct OutData{
//    OutData(): orbit(0), l1a(0), bx(0), fedId(0),
//               bx(0), dccCh(0), bom(0), ampl(0.){}
//    long long orbit;
//    int  l1a;
//    int bx;
//    int fedId;
//    int dccCh;
//    int bom;
//    float ampl;
//  };


  //ctors
public:
  explicit ExtractData(const edm::ParameterSet&);
  ~ExtractData() override;

  void analyze(const edm::Event&, const edm::EventSetup&) override;

  void analyzeEB(const edm::Event&, const edm::EventSetup&) const;
  void analyzeEE(const edm::Event&, const edm::EventSetup&) const;
  void endJob();

  edm::ESWatcher<EcalMappingRcd> watcher_;
  //methods
public:
private:
  void analyzeFed(int fedId);
  void analyzeApd();
  std::string toNth(int n);
  bool decode(const uint32_t* data, int iWord32, std::ostream& out);
  double max(const std::vector<double>& a, unsigned& pos) {
    pos = 0;
    double m = a[pos];
    for (unsigned i = 1; i < a.size(); ++i) {
      if (a[i] > m) {
        m = a[i];
        pos = i;
      }
    }
    return m;
  }
  double min(const std::vector<double>& a) {
    double m = a[0];
    for (unsigned i = 1; i < a.size(); ++i) {
      if (a[i] < m)
        m = a[i];
    }
    return m;
  }
  //static int lme(int dccId1, int side);

  template <class T>
  std::string toString(T val) {
    std::stringstream s;
    s << val;
    return s.str();
  }

  static int sideOfRu(int ru1);

  static int modOfRu(int ru1);

  static int lmodOfRu(int ru1);

  std::string srRange(int offset) const;

  std::string ttfTag(int tccType, unsigned iSeq) const;

  std::string tpgTag(int tccType, unsigned iSeq) const;

  void output_init();
  void write_ch_data();

  //fields
private:
  int verbosity_;
  bool writeDcc_;
  int beg_fed_id_;
  int end_fed_id_;
  int first_event_;
  int last_event_;
  std::string filename_;
  int iEvent_;
  enum {kNone, kCSV, kROOT} out_mode_;

  unsigned iTowerWord64_;
  unsigned iSrWord64_;
  unsigned iTccWord64_;
  enum { inDaqHeader, inDccHeader, inTccBlock, inSrBlock, inTowerBlock } decodeState_;
  size_t towerBlockLength_;

  std::vector<double> adc_;
  std::vector<std::vector<double>> adcs_;
  int bom1_;
  std::vector<int> boms_;
  float ampl_;
  std::vector<float> ampls_;

  static const int nSamples = 10;
  static const int nChsInRu_ = 25;
  double amplCut_;
  bool dump_;
  bool dumpAdc_;
  bool l1aHistory_;
  //  bool doHisto_;
  int maxEvt_;
  int profileFedId_;
  int profileRuId_;
  int l1aMinX_;
  int l1aMaxX_;
  int dccCh_;
  std::vector<uint32_t> lastOrbit_;
  static const unsigned nDccs_ = 54;
  static const unsigned fedStart_ = 601;
  static const int maxTpgsPerTcc_ = 68;
  static const int maxTccsPerDcc_ = 4;

  //@{
  /** TCC types
   */
  static const int ebmTcc_ = 0;
  static const int ebpTcc_ = 1;
  static const int eeInnerTcc_ = 2;
  static const int eeOuterTcc_ = 3;
  static const int nTccTypes_ = 4;
  //@}

  /** TT ID in the order the TPG appears in the data
   */
  static const int ttId_[nTccTypes_][maxTpgsPerTcc_];

  unsigned fedId_;
  unsigned dccId_;
  unsigned side_;
  unsigned eventId_;
  std::vector<unsigned> eventList_;
  std::vector<unsigned> channelList_;
  std::vector<int> orderedFedUnpackList_;
  std::vector<int> orderedDCCIdList_;
  unsigned int numbXtalTSamples_;
  unsigned int numbTriggerTSamples_;
  unsigned minEventId_;
  unsigned maxEventId_;
  unsigned orbit0_;
  uint32_t orbit_;
  bool orbit0Set_;
  int bx_;
  int l1a_;
  int simpleTrigType_;
  int detailedTrigType_;
  //  PGHisto histo_;
  std::vector<std::vector<uint32_t> > l1as_;
  std::vector<std::vector<uint32_t> > orbits_;
  std::vector<std::vector<int> > tpg_;
  int thisTpg_;
  std::vector<int> nTpgs_;
  std::vector<int> dccChStatus_;
  EcalElectronicsMapper* electronicsMapper_;
  bool first_;
  int thisChStatus_;
  int iRu_;
  int srpL1a_;
  int tccL1a_;
  //Number of TPGs in TCC block currently parsed:
  int nTts_;
  //Length of TCC block currently parsed:
  int tccBlockLen64_;
  static const int nRu_ = 70;
  std::vector<int> feL1a_;
  int srpBx_;
  int tccBx_;
  ///type of TCC currently parsed
  int tccType_;
  std::vector<int> feBx_;
  std::vector<int> feRuId_;
  int xtalId_;
  int xtalId_instrip;
  int strip;
  int dcc;
  DetId detId_;
  std::vector<int> xtalId_vec;
  std::vector<int> indx;
  std::vector<int> dccCh_vec;
  std::vector<int> dccChStatus_vec;
  std::vector<int> tpg_vec;
  std::vector<int> fedId_vec;
  int ix_;
  int iy_;
  int iz_;
  float geom_eta_;
  std::vector<int> ix_vec;
  std::vector<int> iy_vec;
  std::vector<int> iz_vec;
  std::vector<float> geom_eta_vec;
  int iTow_;
  std::ofstream dumpFile_;
  bool pulsePerRu_;
  bool pulsePerLmod_;
  bool pulsePerLme_;
  int tccId_;

  edm::ESGetToken<EcalElectronicsMapping, EcalMappingRcd> ecalMappingToken_;
  std::vector<float> map_eta_;
  //tcc sequence number of currenlty parsed tower block of one DCC
  int iTcc_;
  edm::InputTag fedRawDataCollectionTag_;
  edm::InputTag l1AcceptBunchCrossingCollectionTag_;
  edm::EDGetTokenT<FEDRawDataCollection> fedRawDataCollectionToken_;
  edm::EDGetTokenT<L1AcceptBunchCrossingCollection> l1AcceptBunchCrossingCollectionToken_;

  std::unique_ptr<TFile> froot_;
  std::unique_ptr<std::ofstream> fcsv_;
  TTree* tree_;
  //OutData out_data_;

  const edm::InputTag triggerTag_;
  const edm::EDGetTokenT<edm::TriggerResults> triggerToken_;


  // from HLTEventAnalyzerAOD.h
  /// module config parameters
  std::string   processName_;
  edm::InputTag triggerResultsTag_;
  const edm::EDGetTokenT<edm::TriggerResults>   triggerResultsToken_;

  /// HLT trigger names
  edm::ParameterSetID triggerNamesID_;

  /// list of required HLT triggers by HLT name
  std::vector<std::string>  HLTPathsByName_;

  std::vector<int> HLTPathsByIdx_;
};

#endif  //EXTRACTDATA_H not defined
