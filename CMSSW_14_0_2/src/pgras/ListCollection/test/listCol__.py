import FWCore.ParameterSet.Config as cms

process = cms.Process("ListCollection")
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring('/store/mc/RunIISummer16NanoAOD/THQ_Hincl_13TeV-madgraph-pythia8_TuneCUETP8M1/NANOAODSIM/PUMoriond17_05Feb2018_94X_mcRun2_asymptotic_v2-v1/00000/36AAA8DC-2616-E811-BA2C-441EA1616D30.root'),
)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(1)
)
process.list = cms.EDAnalyzer("ListCollection")

process.p = cms.Path(process.list)
