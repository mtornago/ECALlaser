//emacs settings:-*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil -*-
/*
 * $Id: ListCollection.cc,v 1.1.1.1 2008/06/03 16:15:13 pgras Exp $
 */

#include "pgras/ListCollection/interface/ListCollection.h"

#include <vector>
#include <iostream>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"


using namespace std;

ListCollection::ListCollection(const edm::ParameterSet& iConfig)
{
}


ListCollection::~ListCollection()
{
}

// ------------ method called to analyze the data  ------------
void
ListCollection::analyze(const edm::Event& event, const edm::EventSetup& es)
{
  vector<edm::Provenance const*> prov;
  event.getAllProvenance(prov);
  
  for(unsigned int i=0; i< prov.size(); ++i){
    cout << "Module label: " << prov[i]->moduleLabel() << "\n"
         << "Product instance name: " << prov[i]->productInstanceName() << "\n"
         << "Module Name: "  << prov[i]->moduleLabel() << "\n"
         << "Process Name: " << prov[i]->processName() << "\n"
      //         << "Product type: " << prov[i]->productType() << "\n"
         << "Class name: " << prov[i]->className() << "\n"
         << "Branch name: " << prov[i]->branchName() << "\n"
         << "To get the collection: " << "event.getByLabel(\""
         << prov[i]->moduleLabel() << "\", \""
         << prov[i]->productInstanceName()
         << "\", handle)\n"
         << "\n";
  }

}

