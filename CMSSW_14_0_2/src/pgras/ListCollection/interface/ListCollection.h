/*
 * $Id: ListCollection.h,v 1.1.1.1 2008/06/03 16:15:13 pgras Exp $
 */

#ifndef ANALYZER_MODULE_H
#define ANALYZER_MODULE_H

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDAnalyzer.h"

/**
 */
class ListCollection : public edm::stream::EDAnalyzer<> {
  //ctors
public:
  explicit ListCollection(const edm::ParameterSet&);
  ~ListCollection();


  virtual void analyze(const edm::Event&, const edm::EventSetup&);

  void analyzeEE(const edm::Event& event,
                 const edm::EventSetup& es);

  //methods
public:
private:

  //fields
private:
};

#endif //ANALYZER_MODULE_H not defined
