#ifdef __CLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class ROOT::VecOps::RVec<ROOT::VecOps::RVec<short> >+;
#pragma link C++ class ROOT::VecOps::RVec<ROOT::VecOps::RVec<float> >+;
#pragma link C++ class ROOT::VecOps::RVec<ROOT::VecOps::RVec<double> >+;
#endif
