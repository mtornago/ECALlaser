#ifndef DICTS_HH
#define DICTS_HH

#include "ROOT/RDataFrame.hxx"
namespace dummy_a {
	ROOT::VecOps::RVec<ROOT::VecOps::RVec<short>>  a1;
	ROOT::VecOps::RVec<ROOT::VecOps::RVec<float>>  a2;
	ROOT::VecOps::RVec<ROOT::VecOps::RVec<double>> a3;	
}

#endif
