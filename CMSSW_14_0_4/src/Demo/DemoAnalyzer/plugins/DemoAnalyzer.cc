// -*- C++ -*-
//
// Package:    Demo/DemoAnalyzer
// Class:      DemoAnalyzer
//
/**\class DemoAnalyzer DemoAnalyzer.cc Demo/DemoAnalyzer/plugins/DemoAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Marta Tornago
//         Created:  Thu, 13 Jun 2024 07:35:55 GMT
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "DataFormats/Common/interface/Handle.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "DataFormats/Provenance/interface/EventAuxiliary.h"
#include "DataFormats/DetId/interface/DetId.h"
#include "DataFormats/EcalDetId/interface/EBDetId.h"
#include "DataFormats/EcalDetId/interface/EEDetId.h"
#include "DataFormats/EcalDetId/interface/EcalElectronicsId.h"
#include "DataFormats/CaloRecHit/interface/CaloRecHit.h"
#include "DataFormats/EcalRecHit/interface/EcalRecHit.h"
#include "DataFormats/EcalRecHit/interface/EcalRecHitCollections.h"
#include "DataFormats/EcalDigi/interface/EcalDataFrame.h"
#include "EventFilter/EcalRawToDigi/interface/EcalElectronicsMapper.h"
#include "Geometry/EcalMapping/interface/EcalElectronicsMapping.h"
#include "DataFormats/EcalDigi/interface/EcalDigiCollections.h"
#include "DataFormats/EcalDigi/interface/EcalTriggerPrimitiveDigi.h"
#include "Geometry/EcalMapping/interface/EcalMappingRcd.h"
#include "CondFormats/DataRecord/interface/EcalChannelStatusRcd.h"
#include "CondFormats/EcalObjects/interface/EcalChannelStatus.h"
#include "FWCore/Framework/interface/ESWatcher.h"
#include "DataFormats/Provenance/interface/Timestamp.h"
#include "CalibCalorimetry/EcalLaserAnalyzer/interface/MEEBGeom.h"
#include "CalibCalorimetry/EcalLaserAnalyzer/interface/MEEEGeom.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include <ctime>
#include "TTree.h"
#include "TFile.h"
#include "TH1.h"
#include "TMath.h"
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TObjString.h"
#include <iostream>
#include <numeric>
#include <vector>
#include <set>
#include <map>
#include <boost/regex.hpp>
//
// class declaration
//

// If the analyzer does not use TFileService, please remove
// the template argument to the base class so the class inherits
// from  edm::one::EDAnalyzer<>
// This will improve performance in multithreaded jobs.

using namespace std;
using namespace edm;

// forward declarations
class TH1F;
//class TH2D;
class TFile;
//class TTree;


class DemoAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources> {
public:
  explicit DemoAnalyzer(const edm::ParameterSet&);
  ~DemoAnalyzer() override;

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
  
  edm::ESWatcher<EcalMappingRcd> watcher_;

private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // ----------member data ---------------------------
  TFile *rootfile = new TFile("/eos/user/m/mtornago/RecHit_analysis.root","RECREATE");
  TTree *OutTree = new TTree("RecHit","RecHit");
  
  const edm::EDGetTokenT<EBRecHitCollection> ebRecHitToken_;
  const edm::EDGetTokenT<EERecHitCollection> eeRecHitToken_;

  std::vector<int> orderedFedUnpackList_;
  std::vector<int> orderedDCCIdList_;
  unsigned int numbXtalTSamples_;
  unsigned int numbTriggerTSamples_;

  EcalElectronicsMapper* electronicsMapper_;
  bool first_;

  edm::EDGetTokenT<EBDigiCollection> ebDigiCollectionToken_;
  edm::EDGetTokenT<EEDigiCollection> eeDigiCollectionToken_;

  edm::EDGetTokenT<EcalTrigPrimDigiCollection> tpToken_;

  edm::ESGetToken<EcalElectronicsMapping, EcalMappingRcd> ecalMappingToken_;
  
  edm::ESGetToken<EcalChannelStatus, EcalChannelStatusRcd> channelStatusToken_;

  std::vector<float> Hit_energy;
  std::vector<float> Hit_fed;
  std::vector<float> Hit_lm;
  std::vector<float> Hit_ix;
  std::vector<float> Hit_iy;
  std::vector<float> Hit_iz;
  std::vector<float> Hit_ch_status;
  std::vector<float> Digi;
  std::vector<float> Digi_fed;
  std::vector<float> Digi_lm;
  std::vector<float> Digi_ix;
  std::vector<float> Digi_iy;
  std::vector<float> Digi_iz;
  std::vector<float> Digi_ch_status;
  std::vector<float> TP_ampl;
  std::vector<float> TP_lm;
  std::vector<float> TP_ix;
  std::vector<float> TP_iy;
  std::vector<float> TP_iz;

  int n_Hit = 0;
  int run = 0;
  int orbit = 0;
  int lumisection = 0;
  int eventNumber = 0;
  int bx = 0;
  unsigned long long eventTime = 0;

  DetId detId_;
  EcalElectronicsId elecId;
  int fedId;
  int iLM;
  float energy_;
  float ix_;
  float iy_;
  float iz_;
  float digi_;
  float ch_status_;
  float ADC_;

};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
DemoAnalyzer::DemoAnalyzer(const edm::ParameterSet& iConfig)
    : ebRecHitToken_(consumes<EBRecHitCollection>(iConfig.getParameter<edm::InputTag>("EBRecHitColl"))),
      eeRecHitToken_(consumes<EERecHitCollection>(iConfig.getParameter<edm::InputTag>("EERecHitColl"))),
      orderedFedUnpackList_(iConfig.getParameter<std::vector<int> >("orderedFedList")),
      orderedDCCIdList_(iConfig.getParameter<std::vector<int> >("orderedDCCIdList")),
      numbXtalTSamples_(iConfig.getParameter<int>("numbXtalTSamples")),
      numbTriggerTSamples_(iConfig.getParameter<int>("numbTriggerTSamples")),
      electronicsMapper_(nullptr),
      first_(true),
      ebDigiCollectionToken_(consumes<EBDigiCollection>(iConfig.getParameter<edm::InputTag>("EBdigiCollection"))),
      eeDigiCollectionToken_(consumes<EEDigiCollection>(iConfig.getParameter<edm::InputTag>("EEdigiCollection"))),
      tpToken_(consumes<EcalTrigPrimDigiCollection>(iConfig.getParameter<edm::InputTag>("TPdigiCollection"))){
#ifdef THIS_IS_AN_EVENTSETUP_EXAMPLE
  setupDataToken_ = esConsumes<SetupData, SetupRecord>();
#endif
  //now do what ever initialization is needed
  
  channelStatusToken_ = esConsumes<EcalChannelStatus, EcalChannelStatusRcd>();

  ecalMappingToken_ = esConsumes<EcalElectronicsMapping, EcalMappingRcd>();

  electronicsMapper_ = new EcalElectronicsMapper(numbXtalTSamples_, numbTriggerTSamples_);
  bool readResult = electronicsMapper_->makeMapFromVectors(orderedFedUnpackList_, orderedDCCIdList_);

  if (!readResult) {
    std::cout<<"IncorrectConfiguration: Arrays orderedFedList and orderedDCCIdList are emply."
                                     <<"Hard coded correspondence for DCCId:FedId will be used.\n";
  }

}

DemoAnalyzer::~DemoAnalyzer() {
  // do anything here that needs to be done at desctruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called for each event  ------------
void DemoAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
    
    using namespace edm;
    
    Handle<EBRecHitCollection> barrelRecHitsHandle;
    Handle<EERecHitCollection> endcapRecHitsHandle;

    Handle<EBDigiCollection> pEBDigis;
    Handle<EEDigiCollection> pEEDigis;
 
    const auto& ch_status = iSetup.getData(channelStatusToken_);

    EcalChannelStatusMap::const_iterator chit;

    const EBRecHitCollection* EBRecHits = nullptr;
    const EERecHitCollection* EERecHits = nullptr;

    const EBDigiCollection* ebDigis = nullptr;
    const EEDigiCollection* eeDigis = nullptr;

    edm::Handle<EcalTrigPrimDigiCollection> tp;
    iEvent.getByToken(tpToken_, tp);

    iEvent.getByToken(ebRecHitToken_, barrelRecHitsHandle);
    if (!barrelRecHitsHandle.isValid()) {
        LogDebug("") << "EcalRecHit EB Error! can't get product!" << std::endl;
    } else {
        EBRecHits = barrelRecHitsHandle.product();  // get a ptr to the product
    }

    iEvent.getByToken(eeRecHitToken_, endcapRecHitsHandle);
    if (!endcapRecHitsHandle.isValid()) {
        LogDebug("") << "EcalRecHit EE Error! can't get product!" << std::endl;
    } else {
        EERecHits = endcapRecHitsHandle.product();  // get a ptr to the product
    }

    iEvent.getByToken(ebDigiCollectionToken_, pEBDigis);
    ebDigis = pEBDigis.product();  // get a ptr to the produc
    LogInfo("EcalUncalibRecHitInfo") << "total # ebDigis: " << ebDigis->size();

    iEvent.getByToken(eeDigiCollectionToken_, pEEDigis);
    eeDigis = pEEDigis.product();  // get a ptr to the product
    LogInfo("EcalUncalibRecHitInfo") << "total # eeDigis: " << eeDigis->size();

    if (first_) {
     watcher_.check(iSetup);
     ESHandle<EcalElectronicsMapping> ecalmapping = iSetup.getHandle(ecalMappingToken_);
     electronicsMapper_->setEcalElectronicsMapping(ecalmapping.product());
 
     first_ = false;
 
   } else {
     if (watcher_.check(iSetup)) {
       ESHandle<EcalElectronicsMapping> ecalmapping = iSetup.getHandle(ecalMappingToken_);
       electronicsMapper_->deletePointers();
       electronicsMapper_->resetPointers();
       electronicsMapper_->setEcalElectronicsMapping(ecalmapping.product());
     }
   }

    Hit_energy.clear();
    Hit_fed.clear();
    Hit_lm.clear();
    Hit_ix.clear();
    Hit_iy.clear();
    Hit_iz.clear();
    Hit_ch_status.clear(); 
    Digi.clear();
    Digi_fed.clear();
    Digi_lm.clear();
    Digi_ix.clear();
    Digi_iy.clear();
    Digi_iz.clear();
    Digi_ch_status.clear();
    TP_ampl.clear();
    TP_lm.clear();
    TP_ix.clear();
    TP_iy.clear();
    TP_iz.clear();
    n_Hit = 0;

    
    if (EBRecHits) {
        //loop on all EcalRecHits (barrel)
        EBRecHitCollection::const_iterator itb;
        for (itb = EBRecHits->begin(); itb != EBRecHits->end(); ++itb) {
  		
            detId_ = itb->id();
	    elecId = electronicsMapper_->mapping()->getElectronicsId(detId_);
            fedId = 600 + elecId.dccId();
            energy_ = itb->energy();
            EBDetId eid(detId_);
            ix_ = eid.ieta();
            iy_ = eid.iphi();
	    iLM = MEEBGeom::lmr(ix_, iy_);
	    chit = ch_status.getMap().find(eid.rawId());
	    if (chit != ch_status.getMap().end()) {
            	EcalChannelStatusCode ch_code = (*chit);
            	ch_status_ = ch_code.getStatusCode();
	    } else {
   	    	ch_status_ = -1;
	    }
            
            Hit_energy.push_back(energy_);
	    Hit_fed.push_back(fedId);
       	    Hit_lm.push_back(iLM);
       	    Hit_ix.push_back(ix_);
	    Hit_iz.push_back(0);
            Hit_iy.push_back(iy_);
            Hit_ch_status.push_back(ch_status_);

            n_Hit++;
  
        }
    }
    
    if (EERecHits) {
        //loop on all EcalRecHits (barrel)
        EERecHitCollection::const_iterator ite;
        for (ite = EERecHits->begin(); ite != EERecHits->end(); ++ite) {
            
            detId_ = ite->id();
            elecId = electronicsMapper_->mapping()->getElectronicsId(detId_);
            fedId = 600 + elecId.dccId();
	    energy_ = ite->energy();
            EEDetId eid(detId_);
            ix_ = eid.ix();
            iy_ = eid.iy();
            iz_ = eid.zside();
            MEEEGeom::SuperCrysCoord iX = (ix_ - 1) / 5 + 1;
            MEEEGeom::SuperCrysCoord iY = (iy_ - 1) / 5 + 1;
            iLM = MEEEGeom::lmr(iX, iY, eid.zside());
	    chit = ch_status.getMap().find(eid.rawId());
            if (chit != ch_status.getMap().end()) {
                EcalChannelStatusCode ch_code = (*chit);
                ch_status_ = ch_code.getStatusCode();
            } else {
                ch_status_ = -1;
            }

            Hit_energy.push_back(energy_);
            Hit_fed.push_back(fedId);
	    Hit_ix.push_back(ix_);
            Hit_iy.push_back(iy_);
            Hit_iz.push_back(iz_);
	    Hit_lm.push_back(iLM);
	    Hit_ch_status.push_back(ch_status_);

            n_Hit++;
            
        }
    }
  
    if (ebDigis) {
   
	    for (auto itdg = ebDigis->begin(); itdg != ebDigis->end(); ++itdg) {
		    detId_ = itdg->id();
		    elecId = electronicsMapper_->mapping()->getElectronicsId(detId_);
                    fedId = 600 + elecId.dccId();
                    EBDetId eid(detId_);
                    ix_ = eid.ieta();
                    iy_ = eid.iphi();
                    iLM = MEEBGeom::lmr(ix_, iy_);
                    chit = ch_status.getMap().find(eid.rawId());
            	    if (chit != ch_status.getMap().end()) {
                    	EcalChannelStatusCode ch_code = (*chit);
                    	ch_status_ = ch_code.getStatusCode();
            	    } else {
                	ch_status_ = -1;
               	    }

	            Digi_fed.push_back(fedId);
        	    Digi_lm.push_back(iLM);
	            Digi_ix.push_back(ix_);
            	    Digi_iz.push_back(0);
        	    Digi_iy.push_back(iy_);
	            Digi_ch_status.push_back(ch_status_);
    		    
		    for (int iSample = 0; iSample < 10; iSample ++){
                    	digi_ = double(((EcalDataFrame)(*itdg)).sample(iSample).adc());
                    	Digi.push_back(digi_);
                }
            } 
    }

    if (eeDigis) {
	
	    for (auto itdg = eeDigis->begin(); itdg != eeDigis->end(); ++itdg) {
	        detId_ = itdg->id();
                elecId = electronicsMapper_->mapping()->getElectronicsId(detId_);
                fedId = 600 + elecId.dccId();
                EEDetId eid(detId_);
                ix_ = eid.ix();
                iy_ = eid.iy();
                iz_ = eid.zside();
                MEEEGeom::SuperCrysCoord iX = (ix_ - 1) / 5 + 1;
                MEEEGeom::SuperCrysCoord iY = (iy_ - 1) / 5 + 1;
                iLM = MEEEGeom::lmr(iX, iY, eid.zside());
                chit = ch_status.getMap().find(eid.rawId());
                if (chit != ch_status.getMap().end()) {
                	EcalChannelStatusCode ch_code = (*chit);
                	ch_status_ = ch_code.getStatusCode();
                } else {
                	ch_status_ = -1;
                }

                Digi_fed.push_back(fedId);
                Digi_ix.push_back(ix_);
                Digi_iy.push_back(iy_);
                Digi_iz.push_back(iz_);
                Digi_lm.push_back(iLM);
                Digi_ch_status.push_back(ch_status_);

                for (int iSample = 0; iSample < 10; iSample ++){
                    digi_ = double(((EcalDataFrame)(*itdg)).sample(iSample).adc());
                    Digi.push_back(digi_);
                }
            }
    } 


    for (unsigned int i = 0; i < tp.product()->size(); i++) {
	     EcalTriggerPrimitiveDigi tpDigi = (*(tp.product()))[i];
	     ADC_ = tpDigi.compressedEt(); 
	     TP_ampl.push_back(ADC_);
	     const EcalTrigTowerDetId& towerId = tpDigi.id();
	     iLM = -1;
	     if(towerId.subDet() == EcalBarrel){
	     	ix_ = towerId.ieta();
	        iy_ = towerId.iphi();		
		iLM = MEEBGeom::lmr(ix_, iy_);
	     }else{
	     	ix_ = towerId.ix();     
                iy_ = towerId.iy();
	  	iz_ = towerId.zside();	
		MEEEGeom::SuperCrysCoord iX = (ix_ - 1) / 5 + 1;
                MEEEGeom::SuperCrysCoord iY = (iy_ - 1) / 5 + 1;
                iLM = MEEEGeom::lmr(iX, iY, iz_);
	     }
	     TP_lm.push_back(iLM);
	     TP_ix.push_back(ix_);
	     TP_iy.push_back(iy_);
	     TP_iz.push_back(iz_);
    }    
     
    orbit = iEvent.orbitNumber();
    run = iEvent.id().run();
    lumisection = iEvent.id().luminosityBlock();
    eventNumber = iEvent.id().event();
    eventTime = iEvent.eventAuxiliary().time().value();
    bx = iEvent.eventAuxiliary().bunchCrossing();

    OutTree->Fill();

}

// ------------ method called once each job just before starting event loop  ------------
void DemoAnalyzer::beginJob() {
  // please remove this method if not needed

    rootfile->cd();
    
    OutTree->Branch("eventNumber", &eventNumber, "eventNumber/I");
    OutTree->Branch("eventTime", &eventTime, "eventTime/I");
    OutTree->Branch("run", &run, "run/I");
    OutTree->Branch("lumisection", &lumisection, "lumisection/I");
    OutTree->Branch("bx", &bx, "bx/I");
    OutTree->Branch("n_Hit", &n_Hit, "n_Hit/I");
    OutTree->Branch("Hit_energy", "std::vector<float>", &Hit_energy);
    OutTree->Branch("Hit_fed", "std::vector<float>", &Hit_fed);
    OutTree->Branch("Hit_lm", "std::vector<float>", &Hit_lm);
    OutTree->Branch("Hit_ix", "std::vector<float>", &Hit_ix);
    OutTree->Branch("Hit_iy", "std::vector<float>", &Hit_iy);
    OutTree->Branch("Hit_iz", "std::vector<float>", &Hit_iz);
    OutTree->Branch("Hit_ch_status", "std::vector<float>", &Hit_ch_status);
    OutTree->Branch("Digi", "std::vector<float>", &Digi);
    OutTree->Branch("Digi_fed", "std::vector<float>", &Digi_fed);
    OutTree->Branch("Digi_lm", "std::vector<float>", &Digi_lm);
    OutTree->Branch("Digi_ix", "std::vector<float>", &Digi_ix);
    OutTree->Branch("Digi_iy", "std::vector<float>", &Digi_iy);
    OutTree->Branch("Digi_iz", "std::vector<float>", &Digi_iz);
    OutTree->Branch("Digi_ch_status", "std::vector<float>", &Digi_ch_status);
    OutTree->Branch("TP_ampl", "std::vector<float>", &TP_ampl);
    OutTree->Branch("TP_lm", "std::vector<float>", &TP_lm);
    OutTree->Branch("TP_ix", "std::vector<float>", &TP_ix); //eta nbins 56
    OutTree->Branch("TP_iy", "std::vector<float>", &TP_iy); //phi nbins 72
    OutTree->Branch("TP_iz", "std::vector<float>", &TP_iz);

    return;

}

// ------------ method called once each job just after ending the event loop  ------------
void DemoAnalyzer::endJob() {
  // please remove this method if not needed
    rootfile->Write();
    rootfile->Close();

}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void DemoAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);

  //Specify that only 'tracks' is allowed
  //To use, remove the default given above and uncomment below
  //edm::ParameterSetDescription desc;
  //desc.addUntracked<edm::InputTag>("tracks", edm::InputTag("ctfWithMaterialTracks"));
  //descriptions.addWithDefaultLabel(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(DemoAnalyzer);
