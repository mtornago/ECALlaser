import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras
from Configuration.Eras.Era_Run3_cff import Run3

process = cms.Process("Test")

#############################################
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
process.load("Geometry.EcalMapping.EcalMapping_cfi")
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '140X_dataRun3_Prompt_v2', '')
process.load("Configuration.StandardSequences.MagneticField_cff")
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery=cms.untracked.int32(10000)

############################################

#process.options.numberOfThreads=cms.untracked.uint32(10)

process.RecHitAnalysis = cms.EDAnalyzer('DemoAnalyzer',
                              
                               EBRecHitColl = cms.InputTag("ecalRecHit", "EcalRecHitsEB"),
                               EERecHitColl = cms.InputTag("ecalRecHit", "EcalRecHitsEE"),
                               EBdigiCollection = cms.InputTag("ecalDigis", "ebDigis"),
                               EEdigiCollection = cms.InputTag("ecalDigis","eeDigis"),
                               TPdigiCollection = cms.InputTag("ecalTPSkim"),

                               orderedFedList = cms.vint32(
                                601,
                                602,
                                603,
                                604,
                                605,
                                606,
                                607,
                                608,
                                609,
                                610,
                                611,
                                612,
                                613,
                                614,
                                615,
                                616,
                                617,
                                618,
                                619,
                                620,
                                621,
                                622,
                                623,
                                624,
                                625,
                                626,
                                627,
                                628,
                                629,
                                630,
                                631,
                                632,
                                633,
                                634,
                                635,
                                636,
                                637,
                                638,
                                639,
                                640,
                                641,
                                642,
                                643,
                                644,
                                645,
                                646,
                                647,
                                648,
                                649,
                                650,
                                651,
                                652,
                                653,
                                654
                             ),
                             orderedDCCIdList = cms.vint32(
                                     1,
                                     2,
                                     3,
                                     4,
                                     5,
                                     6,
                                     7,
                                     8,
                                     9,
                                     10,
                                     11,
                                     12,
                                     13,
                                     14,
                                     15,
                                     16,
                                     17,
                                     18,
                                     19,
                                     20,
                                     21,
                                     22,
                                     23,
                                     24,
                                     25,
                                     26,
                                     27,
                                     28,
                                     29,
                                     30,
                                     31,
                                     32,
                                     33,
                                     34,
                                     35,
                                     36,
                                     37,
                                     38,
                                     39,
                                     40,
                                     41,
                                     42,
                                     43,
                                     44,
                                     45,
                                     46,
                                     47,
                                     48,
                                     49,
                                     50,
                                     51,
                                     52,
                                     53,
                                     54
                             ),
                             numbTriggerTSamples = cms.int32(1),
                             numbXtalTSamples = cms.int32(10)
                               )

############################################

process.source = cms.Source("PoolSource",
                            fileNames = cms.untracked.vstring(
                                "file:/eos/user/m/mtornago/reco_379133.root",
                                "file:/eos/user/m/mtornago/reco_379135.root",
                                "file:/eos/user/m/mtornago/reco_379138.root",
                                #"file:/eos/user/m/mtornago/reco_379139.root"
                                )
)

process.p = cms.Path(process.RecHitAnalysis)
