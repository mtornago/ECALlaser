/*
 * compile with
 *    # once, for the generation of dictionaries
 *    rootcling dicts.cc dicts.h LinkDef.h
 *    # everytime the source code changes
 *    g++ -Wall -O3 laser_ana.cc dicts.cc `root-config --libs --cflags` -lfftw3 -lfftw3_threads -o laser_ana
 */
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"
#include "ROOT/RVec.hxx"
#include <vector>
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"

#include "TH2.h"
#include "TF1.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TGraph.h"
#include "TMultiGraph.h"

#include <TColor.h>

using namespace std;

FILE *f;
FILE *f1;

int print_values(int run, int ls, int bx, int evt, int time, ROOT::VecOps::RVec<float> energy, ROOT::VecOps::RVec<float> lm)
{
	float lm_energy[92];
	int lm_nhit[92];

	fprintf(stdout,"%d %d %d %d %d",run, ls, bx, evt, time);
	for(int j=0; j<92; j++){
		lm_nhit[j] = 0;
		lm_energy[j] = 0;
	}
	size_t sz =  energy.size();
	for(size_t i=0; i<sz; i++){
		int indx = lm[i]-1;
		lm_nhit[indx]++;
                lm_energy[indx]+=energy[i];
	}
	for(int j=0; j<92; j++){
                if(lm_nhit[j]!=0) lm_energy[j] = lm_energy[j]/lm_nhit[j];
		fprintf(stdout," %d %f", lm_nhit[j], lm_energy[j]);
        }
	fprintf(stdout,"\n");


	return 1;
}

int print_debug(ROOT::VecOps::RVec<float> digi,ROOT::VecOps::RVec<float> ix,ROOT::VecOps::RVec<float> iy,ROOT::VecOps::RVec<float> iz, ROOT::VecOps::RVec<float> lm)
{
	size_t sz = ix.size();

	for(size_t i=0; i<sz; i++){
			fprintf(stdout, "lm %f ix %f iy %f iz %f ", lm[i], ix[i], iy[i], iz[i]);
			for(size_t s=0; s<12; s++){
				fprintf(stdout, "%f ",digi[i*12+s]);
			}
			fprintf(stdout,"\n");
		}
	fprintf(stdout,"\n");
	return 1;
}

ROOT::VecOps::RVec<float> select_side_indx(ROOT::VecOps::RVec<float> z, int side, ROOT::VecOps::RVec<float> status)
{
	size_t sz = z.size();
	ROOT::VecOps::RVec<float> res;

	for(size_t i=0; i<sz; i++){
		if(status[i]==0){
			if(z[i] == side){
				res.push_back(i);		
			}
		}
	}	
	return res;
}

ROOT::VecOps::RVec<float> sel_geo_indx(ROOT::VecOps::RVec<float> ix,ROOT::VecOps::RVec<float> iy,ROOT::VecOps::RVec<float> iz, int xmin, int xmax, int ymin, int ymax, int zsel, ROOT::VecOps::RVec<float> status)
{
	size_t sz = ix.size();
        ROOT::VecOps::RVec<float> res;

	for(size_t i=0; i<sz; i++){
		if(status[i]==0){
			if(iz[i]==zsel){
				if(ix[i]>=xmin && ix[i]<xmax){
					if(iy[i]>=ymin && iy[i]<ymax){
						res.push_back(i);
					}
				}
			}
		}
	}
	return res;
}

ROOT::VecOps::RVec<float> select_ch_lm(ROOT::VecOps::RVec<float> ch, ROOT::VecOps::RVec<float> lm, int lm_sel)
{
	size_t sz = ch.size();
        ROOT::VecOps::RVec<float> res;

	for(size_t i=0; i<sz; i++){
		if(ch[i]==0){
                	if(lm[i]==lm_sel){
				res.push_back(i);
			}
		}
	}

	return res;
}

ROOT::VecOps::RVec<float> select_lm(ROOT::VecOps::RVec<float> lm, int lm_sel)
{
        size_t sz = lm.size();
        ROOT::VecOps::RVec<float> res;

        for(size_t i=0; i<sz; i++){
                        if(lm[i]==lm_sel){
                                res.push_back(i);
                        }
        }

        return res;
}

ROOT::VecOps::RVec<float> select_chstatus(ROOT::VecOps::RVec<float> ch)
{
        size_t sz = ch.size();
        ROOT::VecOps::RVec<float> res;

        for(size_t i=0; i<sz; i++){
                if(ch[i]==0){
                	res.push_back(i);
                }
        }

        return res;
}

ROOT::VecOps::RVec<float> select_vec(ROOT::VecOps::RVec<float> vec, ROOT::VecOps::RVec<float> indx)
{
        size_t sz = indx.size();
        ROOT::VecOps::RVec<float> res;

        for(size_t i=0; i<sz; i++){
		int index = indx[i];
       		res.push_back(vec[index]);
	}
	//fprintf(stdout,"Hit_vec size: %ld        selected index size: %ld\n", vec.size(), res.size());
        return res;
}

float sum_vec(ROOT::VecOps::RVec<float> vec)
{
        float res = 0;

        for(size_t i=0; i<vec.size(); i++){
                res+=vec[i];
        }
        return res;
}

int digi_debug(ROOT::VecOps::RVec<float> digi, /*ROOT::VecOps::RVec<float> lm_i, ROOT::VecOps::RVec<float> x_i, ROOT::VecOps::RVec<float> y_i, ROOT::VecOps::RVec<float> ind,*/ ROOT::VecOps::RVec<float> vec, ROOT::VecOps::RVec<float> lm, int run, int bx, int event, ROOT::VecOps::RVec<float> ix, ROOT::VecOps::RVec<float> iy, ROOT::VecOps::RVec<float> /*int*/ iz)
{
	//bool print=0;
        //size_t iref = -1;
        size_t ds = ix.size();
/*
	for(size_t i=0; i<ds; i++){

                float digiv = vec[i];
                if(print==1 && i>iref+9){
                        print=0;
                        fprintf(stdout,"\n");
                }
                if(digiv<100 && print==0){
                        print = 1;
                        iref = i;
                }
		int indx = iref/10;
		if(print==1 && i==iref) fprintf(stdout,"Run %d Event %d BX %d Hit # %d LM %f ix %f iy %f iz %f digis",run, event, bx, indx, lm[indx], ix[indx], iy[indx], iz[indx]); 
                if(print==1) fprintf(stdout," %f", digiv);
        }
*/

/*	for(size_t i=0; i<ds; i++){
		for(int s=1; s<11; s++){
		float digiv = vec[i*12+s];
		int indx = ind[i];
		int indx_d = indx*10 + s-1;
		float digi_i = digi[indx_d];
		if(digiv>1000){ 
			fprintf(stdout,"Run %d Event %d BX %d Hit # %d LM size %ld ix size %ld Digi size %ld LM pre %f post %f ix pre %f post %f iy pre %f post %f iz %d ",run, event, bx, indx,lm_i.size(), x_i.size(),digi.size()/10,  lm_i[indx], lm[i], x_i[indx], ix[i], y_i[indx],iy[i],iz);
			fprintf(stdout,"digi pre %f post %f", digiv, digi_i);
			fprintf(stdout,"\n");	
			}
		}
	}
*/

	for(size_t i=0; i<ds; i++){
		if(lm[i]>72){
			fprintf(stdout,"Run %d Event %d BX %d Hit # %ld LM %f ix %f iy %f iz %f digis",run, event, bx, i, lm[i], ix[i], iy[i], iz[i]);
			for(int s=0; s<10; s++){
				float digiv = vec[i*12+s];
				fprintf(stdout," %f", digiv);
			}
			fprintf(stdout,"\n");
		}
	}
	return 1;
}

ROOT::VecOps::RVec<float> select_digi(ROOT::VecOps::RVec<float> vec, ROOT::VecOps::RVec<float> indx)
{
        size_t sz = indx.size();
        ROOT::VecOps::RVec<int> res;

        for(size_t i=0; i<sz; i++){
		int index = indx[i];
		res.push_back(0);
		for(int c=0; c<10; c++){
			float value = vec[index*10+c];
			res.push_back(value);
		}
		res.push_back(0);
	}
        return res;
}

float sel_pedestal(ROOT::VecOps::RVec<float> digi)
{
	float res = 0;
	size_t sz = digi.size()/12;

	for(size_t i=0; i<sz; i++){
		res+=digi[i*12+1];
	}

	res = res/sz;

	return res;
}

ROOT::VecOps::RVec<float> digi_mean(ROOT::VecOps::RVec<float> digi)
{
	size_t sz = digi.size()/12;
	ROOT::VecOps::RVec<float> res;

	for(size_t i=0; i<sz; i++){
		float sum=0;
		for(size_t s=0; s<12; s++){
			sum+=digi[i*12+s];
		}
		sum = sum/10;
		res.push_back(sum);
	}
	return res;
}

ROOT::VecOps::RVec<int> lumi_vec(int lumisection, unsigned long nhit)
{
        ROOT::VecOps::RVec<int> res;

        for(unsigned long i=0; i<nhit; i++){
		res.push_back(lumisection);
        }
        return res;
}

ROOT::VecOps::RVec<float> float_to_vec(ROOT::VecOps::RVec<float> num)
{
        ROOT::VecOps::RVec<float> res;

	size_t sz = num.size();

        for(size_t i=0; i<sz; i++){
		for(size_t s=0; s<12; s++){
	                res.push_back(num[i]);
		}
        }
        return res;
}

ROOT::VecOps::RVec<float> isamples(ROOT::VecOps::RVec<float> digi)
{
	size_t sz = digi.size();
	ROOT::VecOps::RVec<float> res;

	for(size_t cnt=0; cnt<sz; cnt+=12){
		res.push_back(0);
		for(size_t i=0; i<10; i++){
			res.push_back(i);
		}
		res.push_back(9);
	}

	return res;
}

int print_pulses(ROOT::VecOps::RVec<float> digi, int run, int lm, int bx)
{
	size_t sz = digi.size();

	for(size_t cnt=0; cnt<sz; cnt+=10){
	
		fprintf(f,"%d %d %d", run, lm, bx);
	
		for(int i=0; i<10; i++){
			float value = digi[cnt+i];
			fprintf(f," %f", value);
		}
		fprintf(f,"\n");
	}

	return 1;
}

ROOT::VecOps::RVec<ROOT::VecOps::RVec<float>> grid_xtal_samples(ROOT::VecOps::RVec<float> digi)
{
	size_t sz = digi.size();
	ROOT::VecOps::RVec<ROOT::VecOps::RVec<float>> res;
	ROOT::VecOps::RVec<float> xtal_samples;

	for(size_t cnt=0; cnt<sz; cnt+=10){
		
		xtal_samples.clear();
		
		for(size_t i=0; i<10; i++){	
			xtal_samples.push_back(digi[cnt+i]);
		}
		
		res.push_back(xtal_samples);
	}

	return res;
}

ROOT::VecOps::RVec<float> ampl(ROOT::VecOps::RVec<float> digi)
{
        size_t sz = digi.size();
	ROOT::VecOps::RVec<float> res;
	float max, min;

        for(size_t i=0; i<sz; i+=12){
			max=0;
                        min=9999;
                        for(int c=1; c<11; c++){
                                float value = digi[i+c];
                                if(value>max) max=value;
                                if(value<min) min=value;
                        }
			res.push_back(max-min);
	}
	
        return res;
}

int main(int argc, char ** argv)
{
        // option handling
        char filename_in[128]               = "15997_raw.root";
        char dirname_out[128]               = ".";
        int multithread = 1;
	int run = 0;
        for (int i = 1; i < argc; ++i) {
                if (argv[i][0] != '-') continue;
                switch (argv[i][1]) {
                        case 'i':
                                sprintf(filename_in, "%s", argv[++i]);
                                break;
                        case 'o':
                                sprintf(dirname_out, "%s", argv[++i]);
                                break;
                        case 's':
                                multithread = 0;
                                break;
			case 'r':
				run = atoi(argv[++i]);
				break;
                        return 1;
                }
        }

       fprintf(stdout, "#           input file: %s\n", filename_in);

        if (multithread) {
                ROOT::EnableThreadSafety();
                ROOT::EnableImplicitMT();
        }

        gErrorIgnoreLevel = kWarning;
        ROOT::Math::MinimizerOptions::SetDefaultPrintLevel(0);
        ROOT::RDataFrame df("RecHit", filename_in);
        fprintf(stderr, "# --> going to run on %d slot(s).\n", df.GetNSlots());

	f =  fopen("/eos/user/m/mtornago/pulse_out.txt", "w");	
	f1 =  fopen("/eos/user/m/mtornago/LS_LM.txt", "w");

	char name[100];
	
	float lm_eta_min[92];
	float lm_eta_max[92];
	float lm_phi_min[92];
	float lm_phi_max[92];

	for(int i=0; i<92; i++){
        if(i<36){
                lm_eta_min[i] = -85;
                lm_eta_max[i] = 0;
                if(i%2==0){
                        lm_phi_min[i] = i*10;
                        lm_phi_max[i] = i*10 + 21;
                }else{
                        lm_phi_min[i] = (i-1)*10;
                        lm_phi_max[i] = (i-1)*10 + 21;
                }
        }else if(i>=36 && i<72){
                lm_eta_min[i] = 0;
                lm_eta_max[i] = 85;
                if(i%2==0){
                        lm_phi_min[i] = (i-36)*10;
                        lm_phi_max[i] = (i-36)*10 + 21;
                }else{
                        lm_phi_min[i] = (i-37)*10;
                        lm_phi_max[i] = (i-37)*10 + 21;
                }
        }else if(i==72 || i==82){
                lm_eta_min[i] = 50;
                lm_eta_max[i] = 100;
                lm_phi_min[i] = 20;
                lm_phi_max[i] = 60;
        }else if(i==73 || i==74 || i==83 || i==84){
                lm_eta_min[i] = 50;
                lm_eta_max[i] = 100;
                lm_phi_min[i] = 50;
                lm_phi_max[i] = 100;
        }else if(i==80 || i==81 || i==90 || i==91){
                lm_eta_min[i] = 50;
                lm_eta_max[i] = 100;
                lm_phi_min[i] = 0;
                lm_phi_max[i] = 50;
        }else if(i==78 || i==79 || i==88 || i==89){
                lm_eta_min[i] = 0;
                lm_eta_max[i] = 50;
                lm_phi_min[i] = 0;
                lm_phi_max[i] = 50;
        }else if(i==77 || i==87){
                lm_eta_min[i] = 0;
                lm_eta_max[i] = 50;
                lm_phi_min[i] = 20;
                lm_phi_max[i] = 60;
        }else if(i==75 || i==76 || i==85 || i==86){
                lm_eta_min[i] = 0;
                lm_eta_max[i] = 50;
                lm_phi_min[i] = 50;
                lm_phi_max[i] = 100;
        }

	}

	std::vector <int> run_num;
	if(run==0){
		run_num.push_back(379133);
		run_num.push_back(379135);
		run_num.push_back(379138);
	}else{
		run_num.push_back(run);
	}
	for(size_t rn=0; rn<run_num.size(); rn++){
		
 		int i = run_num[rn];	
	/*
	
	char run_str[100];
	sprintf(run_str,"%d",i);

        auto dr = df.Define("sel_run",run_str);

	auto db = dr
                .Define("zero","0")
                .Define("indx_eb",select_side_indx,{"Hit_iz","zero","Hit_ch_status"})
                .Define("EBHit_ix", select_vec, {"Hit_ix", "indx_eb"})
                .Define("EBHit_iy", select_vec, {"Hit_iy", "indx_eb"})
                .Define("EBHit_energy", select_vec, {"Hit_energy", "indx_eb"})
                ;

        TCanvas *c_map_EB =  new TCanvas("c_map_EB");
        auto map_EB = db.Profile2D({"map_EB","",173, -86, 86, 366, 0, 365}, "EBHit_ix", "EBHit_iy","EBHit_energy");
        c_map_EB->cd();
        sprintf(name,"map_xtal_mean_energy_EB_run%d", run);
        map_EB->SetNameTitle(name, name);
        map_EB->SetStats(0);
        map_EB->Draw("COLZ");
        c_map_EB->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/map_EB.png", i));

        auto de = dr
                .Define("one","1")
                .Define("minus_one","-1")
                .Define("indx_ep",select_side_indx,{"Hit_iz","one","Hit_ch_status"})
                .Define("indx_em",select_side_indx,{"Hit_iz","minus_one","Hit_ch_status"})
                .Define("EEHit_ix_ep", select_vec, {"Hit_ix", "indx_ep"})
                .Define("EEHit_ix_em",select_vec, {"Hit_ix", "indx_em"})
                .Define("EEHit_iy_ep", select_vec, {"Hit_iy", "indx_ep"})
                .Define("EEHit_iy_em",select_vec, {"Hit_iy", "indx_em"})
                .Define("EEHit_energy_ep", select_vec, {"Hit_energy", "indx_ep"})
                .Define("EEHit_energy_em",select_vec, {"Hit_energy", "indx_em"})
                ;

        TCanvas *c_map_EEp =  new TCanvas("c_map_EEp");
        auto map_EEp = de.Profile2D({"map_EEp","",101, 0, 100, 101, 0, 100}, "EEHit_ix_ep", "EEHit_iy_ep","EEHit_energy_ep");
        c_map_EEp->cd();
        sprintf(name,"map_xtal_mean_energy_EE+_run%d", run);
        map_EEp->SetNameTitle(name, name);
        map_EEp->SetStats(0);
        map_EEp->Draw("COLZ");
        c_map_EEp->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/map_EEp.png", i));

        TCanvas *c_map_EEm =  new TCanvas("c_map_EEm");
        auto map_EEm = de.Profile2D({"map_EEm","",101, 0, 100, 101, 0, 100}, "EEHit_ix_em", "EEHit_iy_em","EEHit_energy_em");
        c_map_EEm->cd();
        sprintf(name,"map_xtal_mean_energy_EE-_run%d", run);
        map_EEm->SetNameTitle(name, name);
        map_EEm->SetStats(0);
        map_EEm->Draw("COLZ");
        c_map_EEm->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/map_EEm.png", i));

	*/
		fprintf(stdout, "Run #%d\n", i);

		char run_str[100];
                sprintf(run_str,"%d",i);

		for(int l=1; l<93; l++){
			
			fprintf(stdout, "--- LM #%d\n", l);

			char lm_str[100];
			sprintf(lm_str,"%d",l);

			auto dlm = df
				//.Define("digi_debug",digi_debug,{"Digi", "Hit_lm", "run", "bx", "eventNumber", "Hit_ix", "Hit_iy","Hit_iz"})
				.Define("sel_run",run_str)
                                .Filter("run==sel_run")
				.Filter("n_Hit>600")
				.Define("sel_lm",lm_str)
				.Define("sel_indx",select_ch_lm,{"Hit_ch_status", "Hit_lm", "sel_lm"})
				.Define("sel_hit_energy",select_vec,{"Hit_energy","sel_indx"})
				.Define("Hit_energy_sum", sum_vec, {"sel_hit_energy"})
				.Define("sel_hit_eta",select_vec,{"Hit_ix","sel_indx"})
				.Define("sel_hit_phi",select_vec,{"Hit_iy","sel_indx"})
				.Define("sel_hit_z",select_vec,{"Hit_iz","sel_indx"})
				.Define("sel_indx_tp", select_lm, {"TP_lm", "sel_lm"})
				.Define("ampltp",select_vec,{"TP_ampl","sel_indx_tp"})
				.Define("nsel_hit","sel_hit_energy.size()")
				.Filter("nsel_hit>0")
				.Define("lumi_vec",lumi_vec,{"lumisection", "nsel_hit"})
				;

			//auto hdebugdigi = dlm.Histo1D({"hdebugdigi","",1,0,1},"digi_debug");
			//hdebugdigi->GetEntries();

			sprintf(name, "c_lm%02d_run%d",l, i);
			TCanvas *c_lm = new TCanvas(name);
			c_lm->Divide(2,1);
			sprintf(name, "g_lm%02d_nhit",l);
			auto g_lm_nhit = dlm.Histo2D({name, name, 50, 0.5, 50.5, 500, 0, 1000},"lumisection","nsel_hit");
			g_lm_nhit->SetNameTitle(name,name);
			c_lm->cd(1);
			g_lm_nhit->Draw("COLZ");
			c_lm->Update();

			/*float sum;

			if(i==379133){
				fprintf(f1,"%d", l);
				for(int bx=1; bx<=50; bx++){
					sum = 0;		
					for(int by=21; by<=500; by++){
						sum += g_lm_nhit->GetBinContent(g_lm_nhit->GetBin(bx,by));
					}
					if(sum>100) fprintf(f1, " %d", bx);
				}
				fprintf(f1,"\n");
			}*/

			sprintf(name, "g_lm%02d_energy",l);
                        auto g_lm_energy = dlm.Histo2D({name, name, 50, 0.5, 50.5, 100,0,5},"lumi_vec","sel_hit_energy");
                        g_lm_energy->SetNameTitle(name,name);
                        c_lm->cd(2);
                        c_lm->SetLogy();
			g_lm_energy->Draw("COLZ");
                        c_lm->Update();
			
			c_lm->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/LM%02d_Hit_vs_LS.png", i, l));

			
			sprintf(name, "c_lm%02d_run%d_energysum",l, i);
                        TCanvas *c_lm_es = new TCanvas(name);
                        auto g_lm_es = dlm.Histo1D({name, name, 50, 0, 50},"Hit_energy_sum");
                        g_lm_es->SetNameTitle(name,name);
                        c_lm_es->cd(1);
                        g_lm_es->Draw();
			c_lm_es->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/LM%02d_ES.png", i, l));

			auto dd = dlm
				.Filter("nsel_hit>20")
				.Filter("bx<120")
				;

			sprintf(name,"c_bx_lm%02d_run%d",l, i);
			TCanvas *c_bx = new TCanvas(name);
			sprintf(name, "g_lm%02d_nhit_vs_bx",l);
			auto g_hit_bx = dd.Histo2D({name, name, 120, 0.5, 120.5, 500, 0, 1000},"bx","nsel_hit");
			g_hit_bx->SetNameTitle(name,name);
			g_hit_bx->Draw("COLZ");
			c_bx->Update();
			c_bx->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/LM%02d_Hit_vs_BX.png", i, l));
			
			
			 //print output files with digi samples for each hit
		/*	 auto dp = dd
				.Filter("bx>15 && bx<30 || bx>55 && bx<70 || bx>80 && bx<100")
				.Define("sel_indx_digi",select_ch_lm,{"Digi_ch_status", "Digi_lm", "sel_lm"})
				.Define("sel_hit_digi",select_digi,{"Digi","sel_indx_digi"})
				.Define("print_pulses", print_pulses, {"sel_hit_digi","run","sel_lm","bx"})
				;
			auto hdp = dp.Histo1D({"histo","title",1,0,1},"print_pulses");
			hdp->GetEntries();
		*/	

			auto d20_tmp = dd
                                 .Filter("bx>15 && bx<30")
				 .Define("sel_indx_digi",select_ch_lm,{"Digi_ch_status", "Digi_lm", "sel_lm"})
                        	 .Define("sel_hit_digi_tmp",select_digi,{"Digi","sel_indx_digi"})
				 .Define("sample_0", sel_pedestal, {"sel_hit_digi_tmp"})
				 ;

			auto mean_p_20 = d20_tmp.Mean<float>("sample_0").GetValue();
			char str_ped[100];
			sprintf(str_ped,"%f",mean_p_20); 

			auto d20 = d20_tmp
				 .Define("pedestal",str_ped)
				 .Define("sel_hit_digi", "sel_hit_digi_tmp-pedestal")
				 .Define("isamples", isamples,{"sel_hit_digi"})
				 .Define("ampl",ampl,{"sel_hit_digi"})
				 .Define("nsel_hit_digi","ampl.size()")
				 .Define("lumi_vec_digi",lumi_vec,{"lumisection", "nsel_hit_digi"})
				 ;
                        auto d60_tmp = dd
                                 .Filter("bx>55 && bx<70")
				 .Define("sel_indx_digi",select_ch_lm,{"Digi_ch_status", "Digi_lm", "sel_lm"})
				 .Define("sel_hit_digi_tmp",select_digi,{"Digi","sel_indx_digi"})
				 .Define("sample_0", sel_pedestal, {"sel_hit_digi_tmp"})
                                 ;

                        auto mean_p_60 = d60_tmp.Mean<float>("sample_0").GetValue();
                        sprintf(str_ped,"%f",mean_p_60);

                        auto d60 = d60_tmp
				 .Define("pedestal",str_ped)
                                 .Define("sel_hit_digi", "sel_hit_digi_tmp-pedestal")
				 .Define("isamples", isamples,{"sel_hit_digi"})
				 .Define("ampl",ampl,{"sel_hit_digi"})
				 .Define("nsel_hit_digi","ampl.size()")
                                 .Define("lumi_vec_digi",lumi_vec,{"lumisection", "nsel_hit_digi"})
				 ;
			auto d90_tmp = dd
				.Filter("bx>80 && bx<100")
				.Define("sel_indx_digi",select_ch_lm,{"Digi_ch_status", "Digi_lm", "sel_lm"})
				.Define("sel_hit_digi_tmp",select_digi,{"Digi","sel_indx_digi"})
				.Define("sample_0", sel_pedestal, {"sel_hit_digi_tmp"})
                                 ;

                        auto mean_p_90 = d90_tmp.Mean<float>("sample_0").GetValue();
                        sprintf(str_ped,"%f",mean_p_90);

                        auto d90 = d90_tmp
                                .Define("pedestal",str_ped)
                                .Define("sel_hit_digi", "sel_hit_digi_tmp-pedestal")
				.Define("isamples", isamples,{"sel_hit_digi"})
				.Define("ampl",ampl,{"sel_hit_digi"})
				.Define("nsel_hit_digi","ampl.size()")
                                .Define("lumi_vec_digi",lumi_vec,{"lumisection", "nsel_hit_digi"})
				;

			sprintf(name,"c_digi_run%d_lm%d",i,l);
			TCanvas *c_digi1 = new TCanvas(name);
			c_digi1->Divide(3,1);
			auto g20 = d20.Graph("isamples","sel_hit_digi");
			sprintf(name,"pulse_bx20_run%d_lm%d",i, l);
			g20->SetNameTitle(name,name);
			c_digi1->cd(1);
			g20->Draw("AL");
			c_digi1->Update();
			auto g60 = d60.Graph("isamples","sel_hit_digi");
			sprintf(name,"pulse_bx60_run%d_lm%d",i, l);
			g60->SetNameTitle(name,name);
			c_digi1->cd(2);
			g60->Draw("AL");
			c_digi1->Update();
			auto g90 = d90.Graph("isamples","sel_hit_digi");
			sprintf(name,"pulse_bx90_run%d_lm%d",i, l);
			g90->SetNameTitle(name,name);
			c_digi1->cd(3);
			g90->Draw("AL");
			c_digi1->Update();
			c_digi1->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Digi_LM%02d_selBX.png", i, l));

			sprintf(name,"c_digi_energy_run%d_lm%d",i,l);
                        TCanvas *c_digi_energy = new TCanvas(name);
                        c_digi_energy->Divide(3,3);
			auto h20 = d20.Histo1D({"digi_ampl_bx25","digi_ampl_bx25", 50,0,50}, "ampl");
			c_digi_energy->cd(1);
			h20->Draw();
			auto h60 = d60.Histo1D({"digi_ampl_bx65","digi_ampl_bx65", 50,0,50}, "ampl");
                        c_digi_energy->cd(2);
                        h60->Draw();
			auto h90 = d90.Histo1D({"digi_ampl_bx90","digi_ampl_bx90", 50,0,50}, "ampl");
                        c_digi_energy->cd(3);
                        h90->Draw();
			auto h20tp = d20.Histo1D({"digi_ampltp_bx25","digi_ampltp_bx25", 17,0,16}, "ampltp");
                        c_digi_energy->cd(4);
			c_digi_energy->SetLogy();
                        h20tp->Draw();
                        auto h60tp = d60.Histo1D({"digi_ampltp_bx65","digi_ampltp_bx65", 17,0,16}, "ampltp");
                        c_digi_energy->cd(5);
			c_digi_energy->SetLogy();
                        h60tp->Draw();
                        auto h90tp = d90.Histo1D({"digi_ampltp_bx90","digi_ampltp_bx90", 17,0,16}, "ampltp");
                        c_digi_energy->cd(6);
			c_digi_energy->SetLogy();
                        h90tp->Draw();
			auto he20 = d20.Histo1D({"digi_energy_bx25","digi_energy_bx25", 100, 0, 0.5}, "sel_hit_energy");
                        c_digi_energy->cd(7);
                        he20->Draw();
                        auto he60 = d60.Histo1D({"digi_energy_bx65","digi_energy_bx65", 100, 0, 0.5}, "sel_hit_energy");
                        c_digi_energy->cd(8);
                        he60->Draw();
                        auto he90 = d90.Histo1D({"digi_energy_bx90","digi_energy_bx90", 100, 0, 0.5}, "sel_hit_energy");
                        c_digi_energy->cd(9);
                        he90->Draw();
      			c_digi_energy->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Digi_LM%02d_selBX_energy.png", i, l));	

			sprintf(name,"c_map_energy_run%d_lm%d",i,l);
                        TCanvas *c_map_energy = new TCanvas(name);
			c_map_energy->Divide(3,2);
			auto h20ma = d20.Histo2D({"map_ampl_bx25","map_ampl_bx25", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_energy->cd(1);
                        h20ma->Draw("COLZ");
                        auto h60ma = d60.Histo2D({"map_ampl_bx65","map_ampl_bx65", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_energy->cd(2);
                        h60ma->Draw("COLZ");
                        auto h90ma = d90.Histo2D({"map_ampl_bx90","map_ampl_bx90", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_energy->cd(3);
			h90ma->Draw("COLZ");
			auto h20ls = d20.Histo2D({"ls_ampl_bx25","ls_ampl_bx25", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_energy->cd(4);
                        h20ls->Draw("COLZ");
                        auto h60ls = d60.Histo2D({"ls_ampl_bx65","ls_ampl_bx65", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_energy->cd(5);
                        h60ls->Draw("COLZ");
                        auto h90ls = d90.Histo2D({"ls_ampl_bx90","ls_ampl_bx90", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_energy->cd(6);
                        h90ls->Draw("COLZ");
			c_map_energy->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Map_LM%02d_selBX_ampl.png", i, l));

			sprintf(name,"c_map_pr_energy_run%d_lm%d",i,l);
                        TCanvas *c_map_pr_energy = new TCanvas(name);
                        c_map_pr_energy->Divide(3,2);
                        auto h20mapr = d20.Profile2D({"map_ampl_bx25pr","map_ampl_bx25pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_pr_energy->cd(1);
                        h20mapr->Draw("COLZ");
                        auto h60mapr = d60.Profile2D({"map_ampl_bx65pr","map_ampl_bx65pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_pr_energy->cd(2);
                        h60mapr->Draw("COLZ");
                        auto h90mapr = d90.Profile2D({"map_ampl_bx90pr","map_ampl_bx90pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], int(lm_phi_max[l-1]-lm_phi_min[l-1]),lm_phi_min[l-1],lm_phi_max[l-1]}, "sel_hit_eta", "sel_hit_phi", "ampl");
                        c_map_pr_energy->cd(3);
                        h90mapr->Draw("COLZ");
                        auto h20lspr = d20.Profile2D({"ls_ampl_bx25pr","ls_ampl_bx25pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_pr_energy->cd(4);
                        h20lspr->Draw("COLZ");
                        auto h60lspr = d60.Profile2D({"ls_ampl_bx65pr","ls_ampl_bx65pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_pr_energy->cd(5);
                        h60lspr->Draw("COLZ");
                        auto h90lspr = d90.Profile2D({"ls_ampl_bx90pr","ls_ampl_bx90pr", int(lm_eta_max[l-1]-lm_eta_min[l-1]),lm_eta_min[l-1],lm_eta_max[l-1], 50, 0.5, 50.5,}, "sel_hit_eta", "lumi_vec_digi", "ampl");
                        c_map_pr_energy->cd(6);
                        h90lspr->Draw("COLZ");
                        c_map_pr_energy->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Map_prof_LM%02d_selBX_ampl.png", i, l));

			sprintf(name,"c_ampl_bx90_run%d_lm%d",i,l);
                        TCanvas *c_bx_90 = new TCanvas(name);
			c_bx_90->Divide(4,2);
			auto d87 = d90.Filter("bx==87");
			auto d88 = d90.Filter("bx==88");
			auto d89 = d90.Filter("bx==89");
			auto d90s = d90.Filter("bx==90");
			auto d91 = d90.Filter("bx==91");
			auto d92 = d90.Filter("bx==92");
			auto d93 = d90.Filter("bx==93");
			auto d94 = d90.Filter("bx==94");
			auto h87 = d87.Histo1D({"ampl_bx_87","ampl_bx_87", 50,0,50}, "ampl");
			c_bx_90->cd(1);
       	                h87->Draw();
			auto h88 = d88.Histo1D({"ampl_bx_88","ampl_bx_88", 50,0,50}, "ampl");
                        c_bx_90->cd(2);
                        h88->Draw();
			auto h89 = d89.Histo1D({"ampl_bx_89","ampl_bx_89", 50,0,50}, "ampl");
                        c_bx_90->cd(3);
                        h89->Draw();
			auto h90s = d90s.Histo1D({"ampl_bx_90","ampl_bx_90", 50,0,50}, "ampl");
                        c_bx_90->cd(4);
                        h90s->Draw();
			auto h91 = d91.Histo1D({"ampl_bx_91","ampl_bx_91", 50,0,50}, "ampl");
                        c_bx_90->cd(5);
                        h91->Draw();
			auto h92 = d92.Histo1D({"ampl_bx_92","ampl_bx_92", 50,0,50}, "ampl");
                        c_bx_90->cd(6);
                        h92->Draw();
			auto h93 = d93.Histo1D({"ampl_bx_93","ampl_bx_93", 50,0,50}, "ampl");
                        c_bx_90->cd(7);
                        h93->Draw();
			auto h94 = d94.Histo1D({"ampl_bx_94","ampl_bx_94", 50,0,50}, "ampl");
                        c_bx_90->cd(8);
                        h94->Draw();
			c_bx_90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Digi_LM%02d_BX90_energy.png", i, l));

			//auto ddebug = d20
			//	.Define("sel_ix",select_vec,{"Hit_ix","sel_indx"})
			//	.Define("sel_iy",select_vec,{"Hit_iy","sel_indx"})
			//	.Define("sel_iz",select_vec,{"Hit_iz","sel_indx"})
			//	.Define("debug",print_debug,{"sel_hit_digi","sel_ix","sel_iy","sel_iz","sel_lm"});	
			//auto hdebug = ddebug.Histo1D({"debug","",1,0,1},"debug");
			//hdebug->GetEntries();
			
		}//lm loop

					
				       auto dhit = df
				       		.Define("sel_run",run_str)
                                                .Filter("run==sel_run")
						.Define("nhit","Digi_lm.size()")
						.Filter("bx<120")
						;

 				       TCanvas *c_nhit_bx = new TCanvas("c_nhit_bx");
                                       auto g_nhit_bx = dhit.Graph("bx","nhit");
                                       g_nhit_bx->Draw("AP");
                                       c_nhit_bx->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/N_hit_digi_vs_bx.png", i));

				       auto dmap = dhit
						.Filter("nhit>20")
						.Define("sel_indx_digi",select_chstatus,{"Digi_ch_status"})
		                                .Define("sel_hit_digi",select_digi,{"Digi","sel_indx_digi"})
                	                        .Define("isamples", isamples,{"sel_hit_digi"})
                              		        .Define("ampl",ampl,{"sel_hit_digi"})
						.Filter("bx>15 && bx<30 || bx>55 && bx<70 || bx>80 && bx<100")
                                                .Define("iz_EB", "0")
						.Define("sel_side_indx_EB",select_side_indx,{"Digi_iz","iz_EB","Digi_ch_status"})
						.Define("sel_hit_digi_EB",select_digi,{"Digi","sel_side_indx_EB"})
						.Define("sel_ix_EB",select_vec,{"Digi_ix","sel_side_indx_EB"})
                                                .Define("sel_iy_EB",select_vec,{"Digi_iy","sel_side_indx_EB"})
						.Define("sel_lm_EB",select_vec,{"Digi_lm","sel_side_indx_EB"})
                                                .Define("digi_mean_EB",digi_mean,{"sel_hit_digi_EB"})
						.Define("ampl_EB",ampl,{"sel_hit_digi_EB"})
						.Define("nhit_xtal_EB", "ampl_EB/ampl_EB")
						.Define("iz_EEm", "-1")
                                                .Define("sel_side_indx_EEm",select_side_indx,{"Digi_iz","iz_EEm","Digi_ch_status"})
                                                .Define("sel_hit_digi_EEm",select_digi,{"Digi","sel_side_indx_EEm"})
                                                .Define("sel_ix_EEm",select_vec,{"Digi_ix","sel_side_indx_EEm"})
                                                .Define("sel_iy_EEm",select_vec,{"Digi_iy","sel_side_indx_EEm"})
						.Define("sel_lm_EEm",select_vec,{"Digi_lm","sel_side_indx_EEm"})
                                                .Define("digi_mean_EEm",digi_mean,{"sel_hit_digi_EEm"})
						.Define("ampl_EEm",ampl,{"sel_hit_digi_EEm"})
						.Define("nhit_xtal_EEm", "ampl_EEm/ampl_EEm")
						.Define("iz_EEp", "1")
                                                .Define("sel_side_indx_EEp",select_side_indx,{"Digi_iz","iz_EEp","Digi_ch_status"})
                                                .Define("sel_hit_digi_EEp",select_digi,{"Digi","sel_side_indx_EEp"})
                                                .Define("sel_ix_EEp",select_vec,{"Digi_ix","sel_side_indx_EEp"})
                                                .Define("sel_iy_EEp",select_vec,{"Digi_iy","sel_side_indx_EEp"})
						.Define("sel_lm_EEp",select_vec,{"Digi_lm","sel_side_indx_EEp"})
                                                .Define("digi_mean_EEp",digi_mean,{"sel_hit_digi_EEp"})
						.Define("ampl_EEp",ampl,{"sel_hit_digi_EEp"})
						.Define("nhit_xtal_EEp", "ampl_EEp/ampl_EEp")
						.Define("lumivec",lumi_vec,{"lumisection","nhit"})
						;


					TCanvas *c_ls_lm = new TCanvas("c_ls_lm");
					auto h_lm_ls = dmap.Histo2D({"lm_vs_ls", "", 50, 0.5, 50.5, 92,0.5,92.5},"lumivec","Digi_lm");
					h_lm_ls->Draw("COLZ");
					c_ls_lm->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/LMshotinLS.png", i));
						
				        auto d20m = dmap.Filter("bx>15 && bx<30");
                                        auto d60m = dmap.Filter("bx>55 && bx<70");
                                        auto d90m = dmap.Filter("bx>80 && bx<100");

					TCanvas *c_ls_lm_bx = new TCanvas("c_ls_lm_bx");
					c_ls_lm_bx->Divide(3,1);
					c_ls_lm_bx->cd(1);
                                        auto h_lm_ls_20 = d20m.Histo2D({"lm_vs_ls_20", "", 50, 0.5, 50.5, 92,0.5,92.5},"lumivec","ampl");
                                        h_lm_ls_20->Draw();
					c_ls_lm_bx->cd(2);
                                        auto h_lm_ls_60 = d60m.Histo2D({"lm_vs_ls_60", "", 50, 0.5, 50.5, 92,0.5,92.5},"lumivec","ampl");
                                        h_lm_ls_60->Draw();
					c_ls_lm_bx->cd(3);
                                        auto h_lm_ls_90 = d90m.Histo2D({"lm_vs_ls_90", "", 50, 0.5, 50.5, 92,0.5,92.5},"lumivec","ampl");
                                        h_lm_ls_90->Draw();
                                        c_ls_lm_bx->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/LMshotinLSvsBX.png", i));
			
					TCanvas *c_map_EB20 =  new TCanvas("c_map_EB_bx20");
                                        auto map_EB20 = d20m.Profile2D({"map_EB20","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","digi_mean_EB");
                                        c_map_EB20->cd();
                                        sprintf(name,"map_xtal_mean_digi_EB_run%d_bx20", i);
                                        map_EB20->SetNameTitle(name, name);
                                        map_EB20->SetStats(0);
                                        map_EB20->Draw("COLZ");
                                        c_map_EB20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EB_bx25.png", i));
					
					TCanvas *c_map_EEp20 =  new TCanvas("c_map_EEp_bx20");
				    	auto map_EEp20 = d20m.Profile2D({"map_EEp20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","digi_mean_EEp");
					c_map_EEp20->cd();
					sprintf(name,"map_xtal_mean_digi_EE+_run%d_bx20", i);
					map_EEp20->SetNameTitle(name, name);
					map_EEp20->SetStats(0);
					map_EEp20->Draw("COLZ");
					c_map_EEp20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEp_bx25.png", i));

					TCanvas *c_map_EEm20 =  new TCanvas("c_map_EEm_bx20");
					auto map_EEm20 = d20m.Profile2D({"map_EEm20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","digi_mean_EEm");
					c_map_EEm20->cd();
					sprintf(name,"map_xtal_mean_digi_EE-_run%d_bx20", i);
					map_EEm20->SetNameTitle(name, name);
					map_EEm20->SetStats(0);
					map_EEm20->Draw("COLZ");
					c_map_EEm20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEm_bx25.png", i));

					TCanvas *c_mapa_EB20 =  new TCanvas("c_mapa_EB_bx20");
                                        auto mapa_EB20 = d20m.Profile2D({"mapa_EB20","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_mapa_EB20->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EB_run%d_bx20", i);
                                        mapa_EB20->SetNameTitle(name, name);
                                        mapa_EB20->SetStats(0);
                                        mapa_EB20->Draw("COLZ");
                                        c_mapa_EB20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EB_bx25.png", i));

                                        TCanvas *c_mapa_EEp20 =  new TCanvas("c_mapa_EEp_bx20");
                                        auto mapa_EEp20 = d20m.Profile2D({"mapa_EEp20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_mapa_EEp20->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EE+_run%d_bx20", i);
                                        mapa_EEp20->SetNameTitle(name, name);
                                        mapa_EEp20->SetStats(0);
                                        mapa_EEp20->Draw("COLZ");
                                        c_mapa_EEp20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEp_bx25.png", i));

                                        TCanvas *c_mapa_EEm20 =  new TCanvas("c_mapa_EEm_bx20");
                                        auto mapa_EEm20 = d20m.Profile2D({"mapa_EEm20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_mapa_EEm20->cd();
                                        sprintf(name,"map_xtal_ampl_EE-_run%d_bx20", i);
                                        mapa_EEm20->SetNameTitle(name, name);
                                        mapa_EEm20->SetStats(0);
                                        mapa_EEm20->Draw("COLZ");
                                        c_mapa_EEm20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEm_bx25.png", i));

					TCanvas *c_ha_EB20 =  new TCanvas("c_ha_EB_bx20");
                                        auto ha_EB20 = d20m.Histo2D({"ha_EB20","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_ha_EB20->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EB_run%d_bx20", i);
                                        ha_EB20->SetNameTitle(name, name);
                                        ha_EB20->SetStats(0);
                                        ha_EB20->Draw("COLZ");
                                        c_ha_EB20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EB_bx25.png", i));

                                        TCanvas *c_ha_EEp20 =  new TCanvas("c_ha_EEp_bx20");
                                        auto ha_EEp20 = d20m.Histo2D({"ha_EEp20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_ha_EEp20->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EE+_run%d_bx20", i);
                                        ha_EEp20->SetNameTitle(name, name);
                                        ha_EEp20->SetStats(0);
                                        ha_EEp20->Draw("COLZ");
                                        c_ha_EEp20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEp_bx25.png", i));

                                        TCanvas *c_ha_EEm20 =  new TCanvas("c_ha_EEm_bx20");
                                        auto ha_EEm20 = d20m.Histo2D({"ha_EEm20","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_ha_EEm20->cd();
                                        sprintf(name,"histo_xtal_ampl_EE-_run%d_bx20", i);
                                        ha_EEm20->SetNameTitle(name, name);
                                        ha_EEm20->SetStats(0);
                                        ha_EEm20->Draw("COLZ");
                                        c_ha_EEm20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEm_bx25.png", i));

                                        TCanvas *c_map_EB60 =  new TCanvas("c_map_EB_bx60");
                                        auto map_EB60 = d60m.Profile2D({"map_EB60","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","digi_mean_EB");
                                        c_map_EB60->cd();
                                        sprintf(name,"map_xtal_mean_digi_EB_run%d_bx60", i);
                                        map_EB60->SetNameTitle(name, name);
                                        map_EB60->SetStats(0);
                                        map_EB60->Draw("COLZ");
                                        c_map_EB60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EB_bx65.png", i));

					TCanvas *c_map_EEp60 =  new TCanvas("c_map_EEp_bx60");
                                        auto map_EEp60 = d60m.Profile2D({"map_EEp60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","digi_mean_EEp");
                                        c_map_EEp60->cd();
                                        sprintf(name,"map_xtal_mean_digi_EE+_run%d_bx60", i);
                                        map_EEp60->SetNameTitle(name, name);
                                        map_EEp60->SetStats(0);
                                        map_EEp60->Draw("COLZ");
                                        c_map_EEp60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEp_bx65.png", i));

                                        TCanvas *c_map_EEm60 =  new TCanvas("c_map_EEm_bx60");
                                        auto map_EEm60 = d60m.Profile2D({"map_EEm60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","digi_mean_EEm");
                                        c_map_EEm60->cd();
                                        sprintf(name,"map_xtal_mean_digi_EE-_run%d_bx60", i);
                                        map_EEm60->SetNameTitle(name, name);
                                        map_EEm60->SetStats(0);
                                        map_EEm60->Draw("COLZ");
                                        c_map_EEm60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEm_bx65.png", i));

					TCanvas *c_mapa_EB60 =  new TCanvas("c_mapa_EB_bx60");
                                        auto mapa_EB60 = d60m.Profile2D({"mapa_EB60","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_mapa_EB60->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EB_run%d_bx60", i);
                                        mapa_EB60->SetNameTitle(name, name);
                                        mapa_EB60->SetStats(0);
                                        mapa_EB60->Draw("COLZ");
                                        c_mapa_EB60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EB_bx65.png", i));

                                        TCanvas *c_mapa_EEp60 =  new TCanvas("c_mapa_EEp_bx60");
                                        auto mapa_EEp60 = d60m.Profile2D({"mapa_EEp60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_mapa_EEp60->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EE+_run%d_bx60", i);
                                        mapa_EEp60->SetNameTitle(name, name);
                                        mapa_EEp60->SetStats(0);
                                        mapa_EEp60->Draw("COLZ");
                                        c_mapa_EEp60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEp_bx65.png", i));

                                        TCanvas *c_mapa_EEm60 =  new TCanvas("c_mapa_EEm_bx60");
                                        auto mapa_EEm60 = d60m.Profile2D({"mapa_EEm60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_mapa_EEm60->cd();
                                        sprintf(name,"map_xtal_ampl_EE-_run%d_bx60", i);
                                        mapa_EEm60->SetNameTitle(name, name);
                                        mapa_EEm60->SetStats(0);
                                        mapa_EEm60->Draw("COLZ");
                                        c_mapa_EEm60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEm_bx65.png", i));


					TCanvas *c_ha_EB60 =  new TCanvas("c_ha_EB_bx60");
                                        auto ha_EB60 = d60m.Histo2D({"ha_EB60","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_ha_EB60->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EB_run%d_bx60", i);
                                        ha_EB60->SetNameTitle(name, name);
                                        ha_EB60->SetStats(0);
                                        ha_EB60->Draw("COLZ");
                                        c_ha_EB60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EB_bx60.png", i));

                                        TCanvas *c_ha_EEp60 =  new TCanvas("c_ha_EEp_bx60");
                                        auto ha_EEp60 = d60m.Histo2D({"ha_EEp60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_ha_EEp60->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EE+_run%d_bx60", i);
                                        ha_EEp60->SetNameTitle(name, name);
                                        ha_EEp60->SetStats(0);
                                        ha_EEp60->Draw("COLZ");
                                        c_ha_EEp60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEp_bx60.png", i));

                                        TCanvas *c_ha_EEm60 =  new TCanvas("c_ha_EEm_bx60");
                                        auto ha_EEm60 = d60m.Histo2D({"ha_EEm60","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_ha_EEm60->cd();
                                        sprintf(name,"histo_xtal_ampl_EE-_run%d_bx60", i);
                                        ha_EEm60->SetNameTitle(name, name);
                                        ha_EEm60->SetStats(0);
                                        ha_EEm60->Draw("COLZ");
                                        c_ha_EEm60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEm_bx60.png", i));

                                        TCanvas *c_map_EB90 =  new TCanvas("c_map_EB_bx90");
                                        auto map_EB90 = d90m.Profile2D({"map_EB90","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","digi_mean_EB");
                                        c_map_EB90->cd();
                                        sprintf(name,"map_xtal_mean_digi_EB_run%d_bx90", i);
                                        map_EB90->SetNameTitle(name, name);
                                        map_EB90->SetStats(0);
                                        map_EB90->Draw("COLZ");
                                        c_map_EB90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EB_bx90.png", i));

					TCanvas *c_map_EEp90 =  new TCanvas("c_map_EEp_bx90");
                                        auto map_EEp90 = d90m.Profile2D({"map_EEp90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","digi_mean_EEp");
                                        c_map_EEp90->cd();
                                        sprintf(name,"map_xtal_mean_digi_EE+_run%d_bx90", i);
                                        map_EEp90->SetNameTitle(name, name);
                                        map_EEp90->SetStats(0);
                                        map_EEp90->Draw("COLZ");
                                        c_map_EEp90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEp_bx90.png", i));

                                        TCanvas *c_map_EEm90 =  new TCanvas("c_map_EEm_bx90");
                                        auto map_EEm90 = d90m.Profile2D({"map_EEm90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","digi_mean_EEm");
                                        c_map_EEm90->cd();
                                        sprintf(name,"map_xtal_mean_digi_EE-_run%d_bx90", i);
                                        map_EEm90->SetNameTitle(name, name);
                                        map_EEm90->SetStats(0);
                                        map_EEm90->Draw("COLZ");
                                        c_map_EEm90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/DigiProfile_EEm_bx90.png", i));

					TCanvas *c_mapa_EB90 =  new TCanvas("c_mapa_EB_bx90");
                                        auto mapa_EB90 = d90m.Profile2D({"mapa_EB90","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_mapa_EB90->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EB_run%d_bx90", i);
                                        mapa_EB90->SetNameTitle(name, name);
                                        mapa_EB90->SetStats(0);
                                        mapa_EB90->Draw("COLZ");
                                        c_mapa_EB90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EB_bx90.png", i));

                                        TCanvas *c_mapa_EEp90 =  new TCanvas("c_mapa_EEp_bx90");
                                        auto mapa_EEp90 = d90m.Profile2D({"mapa_EEp90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_mapa_EEp90->cd();
                                        sprintf(name,"map_xtal_mean_ampl_EE+_run%d_bx90", i);
                                        mapa_EEp90->SetNameTitle(name, name);
                                        mapa_EEp90->SetStats(0);
                                        mapa_EEp90->Draw("COLZ");
                                        c_mapa_EEp90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEp_bx90.png", i));

                                        TCanvas *c_mapa_EEm90 =  new TCanvas("c_mapa_EEm_bx90");
                                        auto mapa_EEm90 = d90m.Profile2D({"mapa_EEm90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_mapa_EEm90->cd();
                                        sprintf(name,"map_xtal_ampl_EE-_run%d_bx90", i);
                                        mapa_EEm90->SetNameTitle(name, name);
                                        mapa_EEm90->SetStats(0);
                                        mapa_EEm90->Draw("COLZ");
                                        c_mapa_EEm90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplProfile_EEm_bx90.png", i));



					TCanvas *c_ha_EB90 =  new TCanvas("c_ha_EB_bx90");
                                        auto ha_EB90 = d90m.Histo2D({"ha_EB90","",173, -86, 86, 366, 0, 365}, "sel_ix_EB", "sel_iy_EB","ampl_EB");
                                        c_ha_EB90->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EB_run%d_bx90", i);
                                        ha_EB90->SetNameTitle(name, name);
                                        ha_EB90->SetStats(0);
                                        ha_EB90->Draw("COLZ");
                                        c_ha_EB90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EB_bx90.png", i));

                                        TCanvas *c_ha_EEp90 =  new TCanvas("c_ha_EEp_bx90");
                                        auto ha_EEp90 = d90m.Histo2D({"ha_EEp90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEp", "sel_iy_EEp","ampl_EEp");
                                        c_ha_EEp90->cd();
                                        sprintf(name,"histo_xtal_mean_ampl_EE+_run%d_bx90", i);
                                        ha_EEp90->SetNameTitle(name, name);
                                        ha_EEp90->SetStats(0);
                                        ha_EEp90->Draw("COLZ");
                                        c_ha_EEp90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEp_bx90.png", i));

                                        TCanvas *c_ha_EEm90 =  new TCanvas("c_ha_EEm_bx90");
                                        auto ha_EEm90 = d90m.Histo2D({"ha_EEm90","",101, 0, 100, 101, 0, 100}, "sel_ix_EEm", "sel_iy_EEm","ampl_EEm");
                                        c_ha_EEm90->cd();
                                        sprintf(name,"histo_xtal_ampl_EE-_run%d_bx90", i);
                                        ha_EEm90->SetNameTitle(name, name);
                                        ha_EEm90->SetStats(0);
                                        ha_EEm90->Draw("COLZ");
                                        c_ha_EEm90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHisto_EEm_bx90.png", i));

					auto dtp = df
						.Define("sel_run",run_str)
                                                .Filter("run==sel_run")
                                                .Define("nhit","TP_lm.size()")
                                                .Filter("bx>15 && bx<30 || bx>55 && bx<70 || bx>80 && bx<100")
						.Define("nhit_TP","TP_ampl/TP_ampl")
						;

					auto d20t = dtp.Filter("bx>15 && bx<30");
                                        auto d60t = dtp.Filter("bx>55 && bx<70");
                                        auto d90t = dtp.Filter("bx>80 && bx<100");

					TCanvas *c_htp_20 =  new TCanvas("c_htp_bx20");
                                        auto htp_20 = d20t.Histo2D({"htp_20","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","nhit_TP");
                                        c_htp_20->cd();
                                        sprintf(name,"histo_TP_nhit_run%d_bx20", i);
                                        htp_20->SetNameTitle(name, name);
                                        htp_20->SetStats(0);
                                        htp_20->Draw("COLZ");
                                        c_htp_20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/nhit_HistoTP_bx20.png", i));

					TCanvas *c_htp_60 =  new TCanvas("c_htp_bx60");
                                        auto htp_60 = d60t.Histo2D({"htp_60","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","nhit_TP");
                                        c_htp_60->cd();
                                        sprintf(name,"histo_TP_nhit_run%d_bx60", i);
                                        htp_60->SetNameTitle(name, name);
                                        htp_60->SetStats(0);
                                        htp_60->Draw("COLZ");
                                        c_htp_60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/nhit_HistoTP_bx60.png", i));

					TCanvas *c_htp_90 =  new TCanvas("c_htp_bx90");
                                        auto htp_90 = d90t.Histo2D({"htp_90","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","nhit_TP");
                                        c_htp_90->cd();
                                        sprintf(name,"histo_TP_nhit_run%d_bx90", i);
                                        htp_90->SetNameTitle(name, name);
                                        htp_90->SetStats(0);
                                        htp_90->Draw("COLZ");
                                        c_htp_90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/nhit_HistoTP_bx90.png", i));
					
					TCanvas *c_ptp_20 =  new TCanvas("c_ptp_bx20");
                                        auto ptp_20 = d20t.Profile2D({"ptp_20","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","TP_ampl");
                                        c_ptp_20->cd();
                                        sprintf(name,"histo_TP_ampl_run%d_bx20", i);
                                        ptp_20->SetNameTitle(name, name);
                                        ptp_20->SetStats(0);
                                        ptp_20->Draw("COLZ");
                                        c_ptp_20->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHistoTP_bx20.png", i));

                                        TCanvas *c_ptp_60 =  new TCanvas("c_ptp_bx60");
                                        auto ptp_60 = d60t.Profile2D({"ptp_90","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","TP_ampl");
                                        c_ptp_60->cd();
                                        sprintf(name,"histo_TP_ampl_run%d_bx60", i);
                                        ptp_60->SetNameTitle(name, name);
                                        ptp_60->SetStats(0);
                                        ptp_60->Draw("COLZ");
                                        c_ptp_60->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHistoTP_bx60.png", i));

                                        TCanvas *c_ptp_90 =  new TCanvas("c_ptp_bx90");
                                        auto ptp_90 = d90t.Profile2D({"ptp_90","", 60, -30, 30, 75, 0, 75}, "TP_ix", "TP_iy","TP_ampl");
                                        c_ptp_90->cd();
                                        sprintf(name,"histo_TP_ampl_run%d_bx90", i);
                                        ptp_90->SetNameTitle(name, name);
                                        ptp_90->SetStats(0);
                                        ptp_90->Draw("COLZ");
                                        c_ptp_90->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/AmplHistoTP_bx90.png", i));

/*

		 for(int k=-85; k<=85; k+=10){
                                for(int j=0; j<=360; j+=10){

					char xmin[100], xmax[100], ymin[100], ymax[100];
					sprintf(xmin,"%d",k);
					sprintf(xmax,"%d",k+10);
					sprintf(ymin,"%d",j);
					sprintf(ymax,"%d",j+10);
                                        sprintf(name, "cdigi_EB_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                        TCanvas *c_digi = new TCanvas(name);
                                        c_digi->Divide(3);
                                        auto dsub = df
						.Define("sel_run",run_str)
                                		.Filter("run==sel_run")
						.Define("iz_sel", "0")
						.Define("xmin",xmin)
						.Define("xmax",xmax)
						.Define("ymin",ymin)
						.Define("ymax",ymax)
						.Define("sel_geo_indx", sel_geo_indx, {"Digi_ix","Digi_iy","Digi_iz","xmin","xmax", "ymin", "ymax","iz_sel","Digi_ch_status"})
						.Define("sel_hit_digi",select_digi,{"Digi","sel_geo_indx"})
						.Define("isamples", isamples,{"sel_hit_digi"})
						;

				       auto d20r = dsub.Filter("bx>15 && bx<30");
				       auto d60r = dsub.Filter("bx>55 && bx<70");
				       auto d90r = dsub.Filter("bx>80 && bx<100");

				       auto g20 = d20r.Graph("isamples","sel_hit_digi");
				       sprintf(name,"pulse_bx20_EB_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                        	       g20->SetNameTitle(name,name);
                        	       c_digi->cd(1);
                        	       g20->Draw("AL");
                        	       c_digi->Update();
                        	       auto g60 = d60r.Graph("isamples","sel_hit_digi");
                        	       sprintf(name,"pulse_bx60_EB_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                        	       g60->SetNameTitle(name,name);
                        	       c_digi->cd(2);
                        	       g60->Draw("AL");
                        	       c_digi->Update();
                        	       auto g90 = d90r.Graph("isamples","sel_hit_digi");
               		               sprintf(name,"pulse_bx90_EB_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
           	             	       g90->SetNameTitle(name,name);
                        	       c_digi->cd(3);
                	               g90->Draw("AL");
        	                       c_digi->Update();
	                               c_digi->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/EBbyregion/Pulses_EB_ix[%d_%d]_iy[%d_%d].png", i, k, k+10, j, j+10));

                                }

                        }


			for(int k=0; k<=100; k+=10){
                                for(int j=0; j<=100; j+=10){
					double d = sqrt((k-50)*(k-50)+(j-50)*(j-50));
					if(d>=20 && d<=50){
                                        char xmin[100], xmax[100], ymin[100], ymax[100];
                                        sprintf(xmin,"%d",k);
                                        sprintf(xmax,"%d",k+10);
                                        sprintf(ymin,"%d",j);
                                        sprintf(ymax,"%d",j+10);
                                        sprintf(name, "cdigi_EEm_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                        TCanvas *c_digi = new TCanvas(name);
                                        c_digi->Divide(3);
                                        auto dsub = df
                                                .Define("sel_run",run_str)
                                                .Filter("run==sel_run")
                                                .Define("iz_sel", "-1")
                                                .Define("xmin",xmin)
                                                .Define("xmax",xmax)
                                                .Define("ymin",ymin)
                                                .Define("ymax",ymax)
                                                .Define("sel_geo_indx", sel_geo_indx, {"Digi_ix","Digi_iy","Digi_iz","xmin","xmax", "ymin", "ymax","iz_sel","Digi_ch_status"})
                                                .Define("sel_hit_digi",select_digi,{"Digi","sel_geo_indx"})
                                                .Define("isamples", isamples,{"sel_hit_digi"});

                                       auto d20r = dsub.Filter("bx>15 && bx<30");
                                       auto d60r = dsub.Filter("bx>55 && bx<70");
                                       auto d90r = dsub.Filter("bx>80 && bx<100");

                                       auto g20 = d20r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx20_EEm_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g20->SetNameTitle(name,name);
                                       c_digi->cd(1);
                                       g20->Draw("AL");
                                       c_digi->Update();
                                       auto g60 = d60r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx60_EEm_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g60->SetNameTitle(name,name);
                                       c_digi->cd(2);
                                       g60->Draw("AL");
                                       c_digi->Update();
                                       auto g90 = d90r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx90_EEm_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g90->SetNameTitle(name,name);
                                       c_digi->cd(3);
                                       g90->Draw("AL");
                                       c_digi->Update();
                                       c_digi->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/EEmbyregion/Pulses_EEm_ix[%d_%d]_iy[%d_%d].png", i, k, k+10, j, j+10));
			        	}
                                }

                        }

			for(int k=0; k<=100; k+=10){
                                for(int j=0; j<=100; j+=10){
                                        double d = sqrt((k-50)*(k-50)+(j-50)*(j-50));
                                        if(d>=20 && d<=50){
                                        char xmin[100], xmax[100], ymin[100], ymax[100];
                                        sprintf(xmin,"%d",k);
                                        sprintf(xmax,"%d",k+10);
                                        sprintf(ymin,"%d",j);
                                        sprintf(ymax,"%d",j+10);
                                        sprintf(name, "cdigi_EEp_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                        TCanvas *c_digi = new TCanvas(name);
                                        c_digi->Divide(3);
                                        auto dsub = df
                                                .Define("sel_run",run_str)
                                                .Filter("run==sel_run")
                                                .Define("iz_sel", "1")
                                                .Define("xmin",xmin)
                                                .Define("xmax",xmax)
                                                .Define("ymin",ymin)
                                                .Define("ymax",ymax)
                                                .Define("sel_geo_indx", sel_geo_indx, {"Digi_ix","Digi_iy","Digi_iz","xmin","xmax", "ymin", "ymax","iz_sel","Digi_ch_status"})
                                                .Define("sel_hit_digi",select_digi,{"Digi","sel_geo_indx"})
                                                .Define("isamples", isamples,{"sel_hit_digi"});

                                       auto d20r = dsub.Filter("bx>15 && bx<30");
                                       auto d60r = dsub.Filter("bx>55 && bx<70");
                                       auto d90r = dsub.Filter("bx>80 && bx<100");

                                       auto g20 = d20r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx20_EEp_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g20->SetNameTitle(name,name);
                                       c_digi->cd(1);
                                       g20->Draw("AL");
                                       c_digi->Update();
                                       auto g60 = d60r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx60_EEp_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g60->SetNameTitle(name,name);
                                       c_digi->cd(2);
                                       g60->Draw("AL");
                                       c_digi->Update();
                                       auto g90 = d90r.Graph("isamples","sel_hit_digi");
                                       sprintf(name,"pulse_bx90_EEp_ix[%d_%d]_iy[%d_%d]", k, k+10, j, j+10);
                                       g90->SetNameTitle(name,name);
                                       c_digi->cd(3);
                                       g90->Draw("AL");
                                       c_digi->Update();
                                       c_digi->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/EEpbyregion/Pulses_EEp_ix[%d_%d]_iy[%d_%d].png", i, k, k+10, j, j+10));
                                        }
                                }

                        } */
		
	} //run loop
/*
	fprintf(stdout,"#Run LS BX Event time");
	for(int i=0; i<92; i++) fprintf(stdout," n_hit_LM%d energy_LM%d", i+1, i+1);
	fprintf(stdout, "\n");
	
	auto ds = df.Define("print",print_values,{"run", "lumisection", "bx", "eventNumber","eventTime", "Hit_energy", "Hit_lm"});

	auto h = ds.Histo1D({"histo","title",1,0,1},"print");
	h->GetEntries();
*/
	fclose(f);
	fclose(f1);

        return 0;
}
