#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TObject.h>
#include <TPaveText.h>
#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"
#include "ROOT/RVec.hxx"


using namespace std;

ROOT::VecOps::RVec<int> isamples()
{
	ROOT::VecOps::RVec<int> res;

	for(int i=0; i<10; i++) res.push_back(i);

	return res;
}

int vec_size(ROOT::VecOps::RVec<int> i, ROOT::VecOps::RVec<float> s)
{
	fprintf(stdout,"size of isample = %ld, size of sample_vec = %ld\n", i.size(), s.size());
	return 1;
}

int main(int argc, char ** argv)
{
	char filename_in[128]  = "/eos/user/m/mtornago/print_pulse.txt";
        int multithread = 1;
	int run=0;
        for (int i = 1; i < argc; ++i) {
                if (argv[i][0] != '-') continue;
                switch (argv[i][1]) {
                        case 'i':
                                sprintf(filename_in, "%s", argv[++i]);
                                break;
  			case 'r':
				run = atoi(argv[++i]);		
  			case 's':
                                multithread = 0;
                                break;
                                return 1;
			default:
	                        fprintf(stderr, "option %s not recognized\n", argv[i]);
	                        return 1;
                }
        }

	if (multithread) {
                ROOT::EnableThreadSafety();
                ROOT::EnableImplicitMT();
        }

/*	ifstream f;
	f.open (filename_in);
	if(!f.is_open()) fprintf(stdout,"Error opening input file. Please check file name");

	TFile * fout = new TFile("/eos/user/m/mtornago/pulse_shapes.root", "RECREATE");
	TTree * t = new TTree("pulses", "pulses");
	int run;
	int lm;
	int bx;
	vector<float> sample_vec;

	t->Branch("run", &run, "run/i");
	t->Branch("lm", &lm, "lm/i");
	t->Branch("bx", &bx, "bx/i");
	t->Branch("sample_vec", "std::vector<float>", &sample_vec);

	string line;
	int row_counter = 0;

	while (getline (f, line)) {
		stringstream s (line);
		string sample;
		int col = 0;
		sample_vec.clear();
		while ((s >> sample)){
			if(col==0) run = stoi(sample);
			if(col==1) lm = stoi(sample);
			if(col==2) bx = stoi(sample);
			if(col>2){
				float f_sample = stof(sample);
				sample_vec.push_back(f_sample);
			}
			col++;
		}
		t->Fill();
		row_counter++;
	}
	
	fout->cd();
	t->Write("", TObject::kOverwrite);
	fout->Close();
*/
	ROOT::RDataFrame df("pulses", "/eos/user/m/mtornago/pulse_shapes.root");
	char name[100];

//	for(int i=379133; i<379139; i++){

//		if(i==379133 || i==379135 || i==379138){
			
			int i = run;

			for(int l=1; l<93; l++){
	
				char run_str[100];
	        	        sprintf(run_str,"%d",i);

				char lm_str[100];
				sprintf(lm_str,"%d",l);

				auto dlm = df
					.Define("sel_run",run_str)
					.Filter("run==sel_run")
					.Define("sel_lm",lm_str)
					.Filter("lm==sel_lm")
					;
				
				auto d20 = dlm
					.Filter("bx>15 && bx<30")
					.Define("isamples", isamples)
					.Define("sizes",vec_size,{"isamples","sample_vec"})
					;
				auto d60 = dlm
					.Filter("bx>55 && bx<70")
					.Define("isamples", isamples)
					.Define("sizes",vec_size,{"isamples","sample_vec"})
                                        ;
				auto d90 = dlm
					.Filter("bx>80 && bx<100")
					.Define("isamples", isamples)
					.Define("sizes",vec_size,{"isamples","sample_vec"})
                                        ;

				sprintf(name,"c_digi_run%d_lm%d",i,l);
                                TCanvas *c_digi = new TCanvas(name);
                                c_digi->Divide(3,1);
				auto g20 = d20.Graph("isamples","sample_vec");
                                sprintf(name,"pulse_bx20_run%d_lm%d",i, l);
                                g20->SetNameTitle(name,name);
                                c_digi->cd(1);
                                g20->Draw("AL");
				c_digi->Update();
				auto g60 = d60.Graph("isamples","sample_vec");
                                sprintf(name,"pulse_bx60_run%d_lm%d",i, l);
                                g60->SetNameTitle(name,name);
                                c_digi->cd(2);
                                g60->Draw("AL");
				c_digi->Update();
				auto g90 = d90.Graph("isamples","sample_vec");
				sprintf(name,"pulse_bx90_run%d_lm%d",i, l);
				g90->SetNameTitle(name,name);
                                c_digi->cd(3);
                                g90->Draw("AL");
				c_digi->Update();	
				c_digi->Print(Form("/eos/user/m/mtornago/www/ECALLaserReco/%d/Digi_LM%02d_selBX.png", i, l));

			}
//		}
//	}

	return 0;


}
